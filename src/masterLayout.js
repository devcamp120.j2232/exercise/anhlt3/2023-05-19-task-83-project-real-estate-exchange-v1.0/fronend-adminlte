"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/master-layouts/";
// biến toàn cục để lưu trữ id đang dc update or delete. Mặc định = 0
var gMLId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gMASTER_LAYOUT_COLS = ["id", "name", "projectId", "acreage", "createDate", "updateDate", "action"];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gML_ID_COL = 0;
const gML_NAME_COL = 1;
const gML_PROJECT_ID_COL = 2;
const gML_ACREAGE_COL = 3;
const gML_CREATE_DATE_COL = 4;
const gML_UPDATE_DATE_COL = 5;
const gML_ACTION_COL = 6;

//khai báo DataTable & mapping columns
var gMLTable = $("#masterlayout-table").DataTable({
  columns: [
    { data: gMASTER_LAYOUT_COLS[gML_ID_COL] },
    { data: gMASTER_LAYOUT_COLS[gML_NAME_COL] },
    { data: gMASTER_LAYOUT_COLS[gML_PROJECT_ID_COL] },
    { data: gMASTER_LAYOUT_COLS[gML_ACREAGE_COL] },
    { data: gMASTER_LAYOUT_COLS[gML_CREATE_DATE_COL] },
    { data: gMASTER_LAYOUT_COLS[gML_UPDATE_DATE_COL] },
    { data: gMASTER_LAYOUT_COLS[gML_ACTION_COL] }
  ],
  columnDefs: [
    {
      targets: gML_ACTION_COL,
      defaultContent: `
            <i class="fas fa-edit edit-ml" style="cursor:pointer;color:blue;"></i>
            <i class="fas fa-trash delete-ml" style="cursor:pointer;color:red;"></i>
          `
    }
  ]
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  onPageLoading();
});
//2 - C: gán sự kiện Create-thêm mới
$("#btn-add-new-masterlayout").on("click", function () {
  onBtnAddNewMLClick();
});
//3 - U: gán sự kiện update-sửa
$("#masterlayout-table").on("click", ".edit-ml", function () {
  onBtnEditMLClick(this);
});
//4 - D: gán sự kiện delete-xóa
$("#masterlayout-table").on("click", ".delete-ml", function () {
  onBtnDeleteMLClick(this);
});

//gán sự kiện cho nút Create(trên modal)
$("#btn-create-masterlayout").on("click", function () {
  onBtnCreateMLClick();
});
//gán sự kiện cho nút Update(trên modal)
$("#btn-update-masterlayout").on("click", function () {
  onBtnUpdateMLClick();
});
//gán sự kiện cho nút Delete(trên modal)
$("#btn-confirm-delete-masterlayout").on("click", function () {
  onBtnDeleteMLConfirmClick();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  //1 - R: read/load to table
  getAllMasterLayouts();
}
//hàm xử lý sự kiện nút Thêm mới
function onBtnAddNewMLClick() {
  //hiển thị modal trắng lên
  $("#create-masterlayout-modal").modal("show");
}
//hàm xử lý sự kiện Update từ table
function onBtnEditMLClick(paramEdit) {
  //lưu thông tin Id đang dc edit vào biến toàn cục
  gMLId = getMLIdFromButton(paramEdit);
  console.log("Id của obj tương ứng = " + gMLId);
  // load dữ liệu vào các trường dữ liệu trong modal
  getMLDataByMLId(gMLId);
}
//hàm xử lý sự kiện Delete từ table
function onBtnDeleteMLClick(paramDelete) {
  //lưu thông tin Id đang dc xóa vào biến toàn cục
  gMLId = getMLIdFromButton(paramDelete);
  console.log("Id của obj tương ứng = " + gMLId);
  $("#delete-masterlayout-modal").modal("show");
}

//hàm xử lý sự kiện nút Insert(trên modal)
function onBtnCreateMLClick() {
  //khai báo obj chứa data
  var vRequestDataObj = {
    name: "",
    description: "",
    projectId: "",
    acreage: "",
    apartmentList: "",
    photo: ""
  }
  //b1: get data
  getInsertMLData(vRequestDataObj);
  //b2: validate data
  var vIsValid = validateMLData(vRequestDataObj);
  //ghi ra console value trả ra của hàm validate
  console.log(vIsValid);
  if (vIsValid) {
    //b3: insert data
    $.ajax({
      url: gBASE_URL,
      type: "POST",
      contentType: "application/json;charset-UTF-8",
      data: JSON.stringify(vRequestDataObj),
      success: function (paramRes) {
        console.log(paramRes);
        //b4: xử lý front-end
        handleInsertMLSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

//hàm xử lý sự kiện nút Update(trên modal)
function onBtnUpdateMLClick() {
  //khai báo obj chứa data
  var vRequestDataObj = {
    name: "",
    description: "",
    projectId: "",
    acreage: "",
    apartmentList: "",
    photo: ""
  }
  //b1: get data
  getUpdateMLData(vRequestDataObj);
  //b2: validate data
  var vIsValid = validateMLData(vRequestDataObj);
  if (vIsValid) {
    //b3: update data
    $.ajax({
      url: gBASE_URL + gMLId,
      type: "PUT",
      contentType: "application/json;charset-UTF-8",
      data: JSON.stringify(vRequestDataObj),
      success: function (paramRes) {
        console.log(paramRes);
        //b4: xử lý front-end
        handleUpdateMLSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

// hàm xử lý sự kiện delete trên modal
function onBtnDeleteMLConfirmClick() {
  $.ajax({
    url: gBASE_URL + gMLId,
    type: "DELETE",
    contentType: "application/json;charset-UTF-8",
    success: function (paramRes) {
      // B4: xử lý front-end
      handleDeleteMLSuccess();
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to DataTable
function loadDataToTable(paramML) {
  gMLTable.clear();
  gMLTable.rows.add(paramML);
  gMLTable.draw();
}
//hàm gọi api để lấy all list
function getAllMasterLayouts() {
  $.ajax({
    url: gBASE_URL,
    type: "GET",
    success: function (paramML) {
      //ghi response ra console
      console.log(paramML);
      loadDataToTable(paramML);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
//hàm dựa vào button detail (edit or delete) xác định dc id obj
function getMLIdFromButton(paramButton) {
  var vTableRow = $(paramButton).parents("tr");
  var vMLRowData = gMLTable.row(vTableRow).data();
  return vMLRowData.id;
}

//hàm thu thập insert data
function getInsertMLData(paramMLObj) {
  paramMLObj.name = $("#inp-create-masterlayout-name").val().trim();
  paramMLObj.description = $("#inp-create-masterlayout-description").val().trim();
  paramMLObj.projectId = $("#inp-create-masterlayout-projectid").val().trim();
  paramMLObj.acreage = $("#inp-create-masterlayout-acreage").val().trim();
  paramMLObj.apartmentList = $("#inp-create-masterlayout-apartlist").val().trim();
  var vPhotoInput = $("#inp-create-masterlayout-photo");
  var vFileName = vPhotoInput.val().split('\\').pop().split('/').pop();
  paramMLObj.photo = vFileName;
}
//hàm validate data
function validateMLData(paramMLObj) {
  if (paramMLObj.name === "") {
    alert("Chưa nhập tên mặt bằng tầng!");
    return false;
  }

  if (paramMLObj.projectId === "") {
    alert("Chưa nhập dự án!");
    return false;
  }

  return true;
}
//hàm xử lý front-end khi thêm obj thành công
function handleInsertMLSuccess() {
  alert("Thêm mới thành công!");
  getAllMasterLayouts();
  resetCreateMLForm();
  $("#create-masterlayout-modal").modal("hide");
}
//hàm xóa trắng form create
function resetCreateMLForm() {
  $("#inp-create-masterlayout-name").val("");
  $("#inp-create-masterlayout-description").val("");
  $("#inp-create-masterlayout-projectid").val("");
  $("#inp-create-masterlayout-acreage").val("");
  $("#inp-create-masterlayout-apartlist").val("");
  $("#inp-create-masterlayout-photo").val("");
}

//hàm load data lên update modal form
function getMLDataByMLId(paramMLId) {
  $.ajax({
    url: gBASE_URL + paramMLId,
    type: "GET",
    success: function (paramMLId) {
      showMLDataToUpdateModal(paramMLId);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
  // hiển thị modal lên
  $("#update-masterlayout-modal").modal("show");
}
//hàm show data lên update modal form
function showMLDataToUpdateModal(paramMLDataObj) {
  $("#inp-update-masterlayout-id").val(paramMLDataObj.id);
  $("#inp-update-masterlayout-name").val(paramMLDataObj.name);
  $("#inp-update-masterlayout-description").val(paramMLDataObj.description);
  $("#inp-update-masterlayout-projectid").val(paramMLDataObj.projectId);
  $("#inp-update-masterlayout-acreage").val(paramMLDataObj.acreage);
  $("#inp-update-masterlayout-apartlist").val(paramMLDataObj.apartmentList);
  // Hiển thị tên file trong một thẻ <span>
  var vFileName = paramMLDataObj.photo.split('/').pop();
  $("#file-label").text(vFileName);
}
//hàm thu thập update data
function getUpdateMLData(paramMLDataObj) {
  paramMLDataObj.name = $("#inp-update-masterlayout-name").val().trim();
  paramMLDataObj.description = $("#inp-update-masterlayout-description").val().trim();
  paramMLDataObj.projectId = $("#inp-update-masterlayout-projectid").val().trim();
  paramMLDataObj.acreage = $("#inp-update-masterlayout-acreage").val().trim();
  paramMLDataObj.apartmentList = $("#inp-update-masterlayout-apartlist").val().trim();
  var vPhotoInput = $("#inp-update-masterlayout-photo");
  var vFileName = vPhotoInput.val().split('\\').pop().split('/').pop();
  paramMLDataObj.photo = vFileName;
}
//hàm xử lý front-end khi update data thành công
function handleUpdateMLSuccess() {
  alert("Cập nhật thành công!");
  getAllMasterLayouts();
  $("#update-masterlayout-modal").modal("hide");
}
//hàm xử lý front-end khi delete thành công
function handleDeleteMLSuccess() {
  alert("Xóa thành công!");
  getAllMasterLayouts();
  $("#delete-masterlayout-modal").modal("hide");
}