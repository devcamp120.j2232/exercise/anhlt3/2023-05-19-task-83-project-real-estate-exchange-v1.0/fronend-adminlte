"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/provinces/";
// biến toàn cục để lưu trữ id class đang dc update or delete. Mặc định = 0
var gProvinceId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gPROVINCE_COLS = ["id", "name", "code", "action"];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gID_COL = 0;
const gNAME_COL = 1;
const gCODE_COL = 2;
const gACTION_COL = 3;

//khai báo DataTable & mapping columns
var gProvinceTable = $("#province-table").DataTable({
  columns: [
    { data: gPROVINCE_COLS[gID_COL] },
    { data: gPROVINCE_COLS[gNAME_COL] },
    { data: gPROVINCE_COLS[gCODE_COL] },
    { data: gPROVINCE_COLS[gACTION_COL] }
  ],
  columnDefs: [
    {
      targets: gACTION_COL,
      defaultContent: `
            <i class="fas fa-edit edit-provice" style="cursor:pointer;color:blue;"></i>
            <i class="fas fa-trash delete-provice" style="cursor:pointer;color:red;"></i>
          `
    }
  ]
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  onPageLoading();
});
//2 - C: gán sự kiện Create-thêm mới
$("#btn-add-new-province").on("click", function () {
  onBtnAddNewProvinceClick();
});
//3 - U: gán sự kiện update-sửa 1
$("#province-table").on("click", ".edit-provice", function () {
  onBtnEditProvinceClick(this);
});
//4 - D: gán sự kiện delete-xóa 1
$("#province-table").on("click", ".delete-provice", function () {
  onBtnDeleteProvinceClick(this);
});

//gán sự kiện cho nút Create(trên modal)
$("#btn-create-province").on("click", function () {
  onBtnCreateProvinceClick();
});
//gán sự kiện cho nút Update(trên modal)
$("#btn-update-province").on("click", function () {
  onBtnUpdateProvinceClick();
});
//gán sự kiện cho nút Delete(trên modal)
$("#btn-confirm-delete-province").on("click", function () {
  onBtnDeleteProvinceConfirmClick();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  //1 - R: read/load to table
  getAllProvinces();
}
//hàm xử lý sự kiện nút Thêm mới
function onBtnAddNewProvinceClick() {
  //hiển thị modal trắng lên
  $("#create-province-modal").modal("show");
}
//hàm xử lý sự kiện Update từ table
function onBtnEditProvinceClick(paramEdit) {
  //lưu thông tin Id đang dc edit vào biến toàn cục
  gProvinceId = getProvinceIdFromButton(paramEdit);
  console.log("Id của obj tương ứng = " + gProvinceId);
  // load dữ liệu vào các trường dữ liệu trong modal
  getProvinceDataByProvinceId(gProvinceId);
}
//hàm xử lý sự kiện Delete từ table
function onBtnDeleteProvinceClick(paramDelete) {
  //lưu thông tin Id đang dc xóa vào biến toàn cục
  gProvinceId = getProvinceIdFromButton(paramDelete);
  console.log("Id của obj tương ứng = " + gProvinceId);
  $("#delete-province-modal").modal("show");
}

//hàm xử lý sự kiện nút Insert(trên modal)
function onBtnCreateProvinceClick() {
  //khai báo obj chứa data
  var vRequestDataObj = {
    name: "",
    code: ""
  }
  //b1: get data
  getInsertProvinceData(vRequestDataObj);
  //b2: validate data
  var vIsValid = validateProvinceData(vRequestDataObj);
  //ghi ra console value trả ra
  console.log(vIsValid);
  if (vIsValid) {
    //b3: insert
    $.ajax({
      url: gBASE_URL,
      type: "POST",
      contentType: "application/json;charset-UTF-8",
      data: JSON.stringify(vRequestDataObj),
      success: function (paramRes) {
        console.log(paramRes);
        //b4: xử lý front-end
        handleInsertProvinceSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

//hàm xử lý sự kiện nút Update(trên modal)
function onBtnUpdateProvinceClick() {
  //khai báo obj chứa data
  var vRequestDataObj = {
    name: "",
    code: ""
  }
  //b1: get data
  getUpdateProvinceData(vRequestDataObj);
  //b2: validate data
  var vIsValid = validateProvinceData(vRequestDataObj);
  if (vIsValid) {
    //b3: update
    $.ajax({
      url: gBASE_URL + gProvinceId,
      type: "PUT",
      contentType: "application/json;charset-UTF-8",
      data: JSON.stringify(vRequestDataObj),
      success: function (paramRes) {
        console.log(paramRes);
        //b4: xử lý front-end
        handleUpdateProvinceSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

// hàm xử lý sự kiện delete trên modal
function onBtnDeleteProvinceConfirmClick() {
  $.ajax({
    url: gBASE_URL + gProvinceId,
    type: "DELETE",
    contentType: "application/json;charset-UTF-8",
    success: function (paramRes) {
      // B4: xử lý front-end
      handleDeleteProvinceSuccess();
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to DataTable
function loadDataToTable(paramProvince) {
  gProvinceTable.clear();
  gProvinceTable.rows.add(paramProvince);
  gProvinceTable.draw();
}
//hàm gọi api để lấy all list
function getAllProvinces() {
  $.ajax({
    url: gBASE_URL,
    type: "GET",
    success: function (paramProvince) {
      //ghi response ra console
      console.log(paramProvince);
      loadDataToTable(paramProvince);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
//hàm dựa vào button detail (edit or delete) xác định dc id obj
function getProvinceIdFromButton(paramButton) {
  var vTableRow = $(paramButton).parents("tr");
  var vProvinceRowData = gProvinceTable.row(vTableRow).data();
  return vProvinceRowData.id;
}

//hàm thu thập insert data
function getInsertProvinceData(paramObj) {
  paramObj.name = $("#inp-create-province-name").val().trim();
  paramObj.code = $("#inp-create-province-code").val().trim();
}
//hàm validate data
function validateProvinceData(paramObj) {
  if (paramObj.name === "") {
    alert("Chưa nhập tên!");
    return false;
  }

  if (paramObj.code === "") {
    alert("Chưa nhập code!");
    return false;
  }
  return true;
}
//hàm xử lý front-end khi thêm obj thành công
function handleInsertProvinceSuccess() {
  alert("Thêm mới thành công!");
  getAllProvinces();
  resetCreateProvinceForm();
  $("#create-province-modal").modal("hide");
}
//hàm xóa trắng form create
function resetCreateProvinceForm() {
  $("#inp-create-province-name").val("");
  $("#inp-create-province-code").val("");
}

//hàm load data lên update modal form
function getProvinceDataByProvinceId(paramId) {
  $.ajax({
    url: gBASE_URL + paramId,
    type: "GET",
    success: function (paramId) {
      showProvinceDataToUpdateModal(paramId);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
  // hiển thị modal lên
  $("#update-province-modal").modal("show");
}
//hàm show data lên update modal form
function showProvinceDataToUpdateModal(paramDataObj) {
  $("#inp-update-province-id").val(paramDataObj.id);
  $("#inp-update-province-name").val(paramDataObj.name);
  $("#inp-update-province-code").val(paramDataObj.code);
}
//hàm thu thập update data
function getUpdateProvinceData(paramDataObj) {
  paramDataObj.name = $("#inp-update-province-name").val().trim();
  paramDataObj.code = $("#inp-update-province-code").val().trim();
}
//hàm xử lý front-end khi update data thành công
function handleUpdateProvinceSuccess() {
  alert("Cập nhật thành công!");
  getAllProvinces();
  $("#update-province-modal").modal("hide");
}
//hàm xử lý front-end khi delete thành công
function handleDeleteProvinceSuccess() {
  alert("Xóa thành công!");
  getAllProvinces();
  $("#delete-province-modal").modal("hide");
}