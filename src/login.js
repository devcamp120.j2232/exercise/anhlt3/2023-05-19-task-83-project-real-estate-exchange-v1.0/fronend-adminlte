$(document).ready(function () {
    //Kiểm tra token nếu có token tức người dùng đã đăng nhập
    const token = getCookie("token");

    if (token) {
        window.location.href = "address-map.html";
    }

    //Sự kiện bấm nút login
    $("#btn-login").on("click", function() {      
        var username = $("#inp-username").val().trim();
        var password = $("#inp-password").val().trim();

        if(validateForm(username, password)) {
            signinForm(username, password);
        }
    });

    function signinForm(username, password) {
        var loginUrl = "http://127.0.0.1:8080/api/auth/signin";

        var vLoginData = {
            username: username,
            password: password
        }

        $.ajax({
            url: loginUrl,
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(vLoginData),
            success: function(responseObject) {
                responseHandler(responseObject);
            },
            error: function(xhr) {
               // Lấy error message
               var errorMessage = xhr.responseJSON.message;
               showError(errorMessage);
            }
        });
    }

    //Xử lý object trả về khi login thành công
    function responseHandler(data) {
        //Lưu token vào cookie trong 1 ngày
        setCookie("token", data.accessToken, 1);

        window.location.href = "address-map.html";
    }

    //Hàm setCookie đã giới thiệu ở bài trước
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    //Hiển thị lỗi lên form 
    function showError(message) {
        var errorElement = $("#error");

        errorElement.html(message);
        errorElement.addClass("d-block");
        errorElement.addClass("d-none");
    }

    //Validate dữ liệu từ form
    function validateForm(username, password) {
        if(username === "") {
            alert("Username không phù hợp");
            return false;
        };

        if(password === "") {
            alert("Password không phù hợp");
            return false;
        }

        return true;
    }

    //Hàm get Cookie 
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
});