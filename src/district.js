"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/districts/";
// biến toàn cục để lưu trữ id class đang dc update or delete. Mặc định = 0
var gDistrictId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gDISTRICT_COLS = ["id", "name", "prefix", "provinceId", "action"];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gID_COL = 0;
const gNAME_COL = 1;
const gPREFIX_COL = 2;
const gPROVINCE_COL = 3;
const gACTION_COL = 4;

//khai báo DataTable & mapping columns
var gDistrictTable = $("#district-table").DataTable({
  columns: [
    { data: gDISTRICT_COLS[gID_COL] },
    { data: gDISTRICT_COLS[gNAME_COL] },
    { data: gDISTRICT_COLS[gPREFIX_COL] },
    {
      data: gDISTRICT_COLS[gPROVINCE_COL],
      render: function (data) {
        return data.name;
      }
    },
    { data: gDISTRICT_COLS[gACTION_COL] }
  ],
  columnDefs: [
    {
      targets: gACTION_COL,
      defaultContent: `
            <i class="fas fa-edit edit-provice" style="cursor:pointer;color:blue;"></i>
            <i class="fas fa-trash delete-provice" style="cursor:pointer;color:red;"></i>
          `
    }
  ]
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  onPageLoading();
});
//2 - C: gán sự kiện Create-thêm mới
$("#btn-add-new-district").on("click", function () {
  onBtnAddNewDistrictClick();
});
//3 - U: gán sự kiện update-sửa 1
$("#district-table").on("click", ".edit-provice", function () {
  onBtnEditDistrictClick(this);
});
//4 - D: gán sự kiện delete-xóa 1
$("#district-table").on("click", ".delete-provice", function () {
  onBtnDeleteDistrictClick(this);
});

//gán sự kiện cho nút Create(trên modal)
$("#btn-create-district").on("click", function () {
  onBtnCreateDistrictClick();
});
//gán sự kiện cho nút Update(trên modal)
$("#btn-update-district").on("click", function () {
  onBtnUpdateDistrictClick();
});
//gán sự kiện cho nút Delete(trên modal)
$("#btn-confirm-delete-district").on("click", function () {
  onBtnDeleteDistrictConfirmClick();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  //1 - R: read/load to table
  getAllDistricts();
}
//hàm xử lý sự kiện nút Thêm mới
function onBtnAddNewDistrictClick() {
  //hiển thị modal trắng lên
  $("#create-district-modal").modal("show");
}
//hàm xử lý sự kiện Update từ table
function onBtnEditDistrictClick(paramEdit) {
  //lưu thông tin Id đang dc edit vào biến toàn cục
  gDistrictId = getDistrictIdFromButton(paramEdit);
  console.log("Id của obj tương ứng = " + gDistrictId);
  // load dữ liệu vào các trường dữ liệu trong modal
  getDistrictDataByDistrictId(gDistrictId);
}
//hàm xử lý sự kiện Delete từ table
function onBtnDeleteDistrictClick(paramDelete) {
  //lưu thông tin Id đang dc xóa vào biến toàn cục
  gDistrictId = getDistrictIdFromButton(paramDelete);
  console.log("Id của obj tương ứng = " + gDistrictId);
  $("#delete-district-modal").modal("show");
}

//hàm xử lý sự kiện nút Insert(trên modal)
function onBtnCreateDistrictClick() {
  //khai báo obj chứa data
  var vRequestDataObj = {
    name: "",
    prefix: "",
    provinceId: {
      id: ""
    }
  }
  //b1: get data
  getInsertDistrictData(vRequestDataObj);
  //b2: validate data
  var vIsValid = validateDistrictData(vRequestDataObj);
  //ghi ra console value trả ra
  console.log(vIsValid);
  if (vIsValid) {
    vRequestDataObj.provinceId = {
      id: parseInt(vRequestDataObj.provinceId)
    };
    //b3: insert
    $.ajax({
      url: gBASE_URL,
      type: "POST",
      contentType: "application/json;charset-UTF-8",
      data: JSON.stringify(vRequestDataObj),
      success: function (paramRes) {
        console.log(paramRes);
        //b4: xử lý front-end
        handleInsertDistrictSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

//hàm xử lý sự kiện nút Update(trên modal)
function onBtnUpdateDistrictClick() {
  //khai báo obj chứa data
  var vRequestDataObj = {
    name: "",
    prefix: "",
    provinceId: {
      id: ""
    }
  }
  //b1: get data
  getUpdateDistrictData(vRequestDataObj);
  //b2: validate data
  var vIsValid = validateDistrictData(vRequestDataObj);
  if (vIsValid) {
    vRequestDataObj.provinceId = {
      id: parseInt(vRequestDataObj.provinceId)
    };
    //b3: update
    $.ajax({
      url: gBASE_URL + gDistrictId,
      type: "PUT",
      contentType: "application/json;charset-UTF-8",
      data: JSON.stringify(vRequestDataObj),
      success: function (paramRes) {
        console.log(paramRes);
        //b4: xử lý front-end
        handleUpdateDistrictSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

// hàm xử lý sự kiện delete trên modal
function onBtnDeleteDistrictConfirmClick() {
  $.ajax({
    url: gBASE_URL + gDistrictId,
    type: "DELETE",
    contentType: "application/json;charset-UTF-8",
    success: function (paramRes) {
      // B4: xử lý front-end
      handleDeleteDistrictSuccess();
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to DataTable
function loadDataToTable(paramDistrict) {
  gDistrictTable.clear();
  gDistrictTable.rows.add(paramDistrict);
  gDistrictTable.draw();
}
//hàm gọi api để lấy all list
function getAllDistricts() {
  $.ajax({
    url: gBASE_URL,
    type: "GET",
    success: function (paramDistrict) {
      //ghi response ra console
      console.log(paramDistrict);
      loadDataToTable(paramDistrict);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
//hàm dựa vào button detail (edit or delete) xác định dc id obj
function getDistrictIdFromButton(paramButton) {
  var vTableRow = $(paramButton).parents("tr");
  var vDistrictRowData = gDistrictTable.row(vTableRow).data();
  return vDistrictRowData.id;
}

//hàm thu thập insert data
function getInsertDistrictData(paramObj) {
  paramObj.name = $("#inp-create-district-name").val().trim();
  paramObj.prefix = $("#select-create-prefix").val();
  paramObj.provinceId = $("#select-create-province").val();
}
//hàm validate data
function validateDistrictData(paramObj) {
  if (paramObj.name === "") {
    alert("Chưa nhập tên!");
    return false;
  }
  return true;
}
//hàm xử lý front-end khi thêm obj thành công
function handleInsertDistrictSuccess() {
  alert("Thêm mới thành công!");
  getAllDistricts();
  resetCreateDistrictForm();
  $("#create-district-modal").modal("hide");
}
//hàm xóa trắng form create
function resetCreateDistrictForm() {
  $("#inp-create-district-name").val("");
  $("#select-create-prefix").val("");
  $("#select-create-province").val("");
}

//hàm load data lên update modal form
function getDistrictDataByDistrictId(paramId) {
  $.ajax({
    url: gBASE_URL + paramId,
    type: "GET",
    success: function (paramId) {
      showDistrictDataToUpdateModal(paramId);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
  // hiển thị modal lên
  $("#update-district-modal").modal("show");
}
//hàm show data lên update modal form
function showDistrictDataToUpdateModal(paramDataObj) {
  $("#inp-update-district-id").val(paramDataObj.id);
  $("#inp-update-district-name").val(paramDataObj.name);
  $("#select-update-prefix").val(paramDataObj.prefix);

  $("#select-update-province option").each(function () {
    // Kiểm tra giá trị option có khớp với provinceId hay không
    if ($(this).val() == paramDataObj.provinceId.id) {
      // Đặt selected cho tùy chọn khớp
      $(this).prop("selected", true);
    }
  });
}
//hàm thu thập update data
function getUpdateDistrictData(paramDataObj) {
  paramDataObj.name = $("#inp-update-district-name").val().trim();
  paramDataObj.prefix = $("#select-update-prefix").val();
  paramDataObj.provinceId = $("#select-update-province").val();
}
//hàm xử lý front-end khi update data thành công
function handleUpdateDistrictSuccess() {
  alert("Cập nhật thành công!");
  getAllDistricts();
  $("#update-district-modal").modal("hide");
}
//hàm xử lý front-end khi delete thành công
function handleDeleteDistrictSuccess() {
  alert("Xóa thành công!");
  getAllDistricts();
  $("#delete-district-modal").modal("hide");
}