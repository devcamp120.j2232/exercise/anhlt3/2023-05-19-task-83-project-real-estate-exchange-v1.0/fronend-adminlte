"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/design-units/";
// biến toàn cục để lưu trữ id đang dc update or delete. Mặc định = 0
var gDUId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gDU_COLS = ["id", "name", "address", "phone", "phone2", "fax", "email", "action"];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gID_COL = 0;
const gNAME_COL = 1;
const gADDRESS_COL = 2;
const gPHONE_COL = 3;
const gPHONE2_COL = 4;
const gFAX_COL = 5;
const gEMAIL_COL = 6;
const gACTION_COL = 7;

//khai báo DataTable & mapping columns
var gDUTable = $("#designunit-table").DataTable({
    columns: [
        { data: gDU_COLS[gID_COL] },
        { data: gDU_COLS[gNAME_COL] },
        { data: gDU_COLS[gADDRESS_COL] },
        { data: gDU_COLS[gPHONE_COL] },
        { data: gDU_COLS[gPHONE2_COL] },
        { data: gDU_COLS[gFAX_COL] },
        { data: gDU_COLS[gEMAIL_COL] },
        { data: gDU_COLS[gACTION_COL] }
    ],
    columnDefs: [
        {
            targets: gACTION_COL,
            defaultContent: `
            <i class="fas fa-edit edit-designunit" style="cursor:pointer;color:blue;"></i>
            <i class="fas fa-trash delete-designunit" style="cursor:pointer;color:red;"></i>
          `
        }
    ]
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
});
//2 - C: gán sự kiện Create-thêm mới
$("#btn-add-new-designunit").on("click", function () {
    onBtnAddNewDUClick();
});
//3 - U: gán sự kiện update-sửa
$("#designunit-table").on("click", ".edit-designunit", function () {
    onBtnEditDUClick(this);
});
//4 - D: gán sự kiện delete-xóa
$("#designunit-table").on("click", ".delete-designunit", function () {
    onBtnDeleteDUClick(this);
});

//gán sự kiện cho nút Create(trên modal)
$("#btn-create-designunit").on("click", function () {
    onBtnCreateDUClick();
});
//gán sự kiện cho nút Update(trên modal)
$("#btn-update-designunit").on("click", function () {
    onBtnUpdateDUClick();
});
//gán sự kiện cho nút Delete(trên modal)
$("#btn-confirm-delete-designunit").on("click", function () {
    onBtnDeleteDUConfirmClick();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    //1 - R: read/load to table
    getAllDesignUnits();
}
//hàm xử lý sự kiện nút Thêm mới
function onBtnAddNewDUClick() {
    //hiển thị modal trắng lên
    $("#create-designunit-modal").modal("show");
}
//hàm xử lý sự kiện Update từ table
function onBtnEditDUClick(paramEdit) {
    //lưu thông tin Id đang dc edit vào biến toàn cục
    gDUId = getDUIdFromButton(paramEdit);
    console.log("Id của obj tương ứng = " + gDUId);
    // load dữ liệu vào các trường dữ liệu trong modal
    getDUDataByDUId(gDUId);
}
//hàm xử lý sự kiện Delete từ table
function onBtnDeleteDUClick(paramDelete) {
    //lưu thông tin Id đang dc xóa vào biến toàn cục
    gDUId = getDUIdFromButton(paramDelete);
    console.log("Id của obj tương ứng = " + gDUId);
    $("#delete-designunit-modal").modal("show");
}

//hàm xử lý sự kiện nút Insert(trên modal)
function onBtnCreateDUClick() {
    //khai báo obj chứa data
    var vRequestDataObj = {
        name: "",
        description: "",
        projects: "",
        address: "",
        phone: "",
        phone2: "",
        fax: "",
        email: "",
        website: "",
        note: ""
    }
    //b1: get data
    getInsertDUData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateDUData(vRequestDataObj);
    //ghi ra console value trả ra của hàm validate
    console.log(vIsValid);
    if (vIsValid) {
        //b3: insert data
        $.ajax({
            url: gBASE_URL,
            type: "POST",
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleInsertDUSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

//hàm xử lý sự kiện nút Update(trên modal)
function onBtnUpdateDUClick() {
    //khai báo obj chứa data
    var vRequestDataObj = {
        name: "",
        description: "",
        projects: "",
        address: "",
        phone: "",
        phone2: "",
        fax: "",
        email: "",
        website: "",
        note: ""
    }
    //b1: get data
    getUpdateDUData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateDUData(vRequestDataObj);
    if (vIsValid) {
        //b3: update data
        $.ajax({
            url: gBASE_URL + gDUId,
            type: "PUT",
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleUpdateDUSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

// hàm xử lý sự kiện delete trên modal
function onBtnDeleteDUConfirmClick() {
    $.ajax({
        url: gBASE_URL + gDUId,
        type: "DELETE",
        contentType: "application/json;charset-UTF-8",
        success: function (paramRes) {
            // B4: xử lý front-end
            handleDeleteDUSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to DataTable
function loadDataToTable(paramDU) {
    gDUTable.clear();
    gDUTable.rows.add(paramDU);
    gDUTable.draw();
}
//hàm gọi api để lấy all list
function getAllDesignUnits() {
    $.ajax({
        url: gBASE_URL,
        type: "GET",
        success: function (paramDU) {
            //ghi response ra console
            console.log(paramDU);
            loadDataToTable(paramDU);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
//hàm dựa vào button detail (edit or delete) xác định dc id obj
function getDUIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    var vDURowData = gDUTable.row(vTableRow).data();
    return vDURowData.id;
}

//hàm thu thập insert data
function getInsertDUData(paramDUObj) {
    paramDUObj.name = $("#inp-create-designunit-name").val().trim();
    paramDUObj.description = $("#inp-create-designunit-description").val().trim();
    paramDUObj.projects = $("#inp-create-designunit-projects").val().trim();
    paramDUObj.address = $("#inp-create-designunit-address").val().trim();
    paramDUObj.phone = $("#inp-create-designunit-phone").val().trim();
    paramDUObj.phone2 = $("#inp-create-designunit-phone2").val().trim();
    paramDUObj.fax = $("#inp-create-designunit-fax").val().trim();
    paramDUObj.email = $("#inp-create-designunit-email").val().trim();
    paramDUObj.website = $("#inp-create-designunit-website").val().trim();
    paramDUObj.note = $("#inp-create-designunit-note").val().trim();
}
//hàm validate data
function validateDUData(paramDUObj) {
    if (paramDUObj.name === "") {
        alert("Chưa nhập tên đơn vị thiết kế!");
        return false;
    }

    return true;
}
//hàm xử lý front-end khi thêm obj thành công
function handleInsertDUSuccess() {
    alert("Thêm mới thành công!");
    getAllDesignUnits();
    resetCreateDUForm();
    $("#create-designunit-modal").modal("hide");
}
//hàm xóa trắng form create
function resetCreateDUForm() {
    $("#inp-create-designunit-name").val("");
    $("#inp-create-designunit-description").val("");
    $("#inp-create-designunit-projects").val("");
    $("#inp-create-designunit-address").val("");
    $("#inp-create-designunit-phone").val("");
    $("#inp-create-designunit-phone2").val("");
    $("#inp-create-designunit-fax").val("");
    $("#inp-create-designunit-email").val("");
    $("#inp-create-designunit-website").val("");
    $("#inp-create-designunit-note").val("");
}

//hàm load data lên update modal form
function getDUDataByDUId(paramDUId) {
    $.ajax({
        url: gBASE_URL + paramDUId,
        type: "GET",
        success: function (paramDUId) {
            showDUDataToUpdateModal(paramDUId);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
    // hiển thị modal lên
    $("#update-designunit-modal").modal("show");
}
//hàm show data lên update modal form
function showDUDataToUpdateModal(paramDUDataObj) {
    $("#inp-update-designunit-id").val(paramDUDataObj.id);
    $("#inp-update-designunit-name").val(paramDUDataObj.name);
    $("#inp-update-designunit-description").val(paramDUDataObj.description);
    $("#inp-update-designunit-projects").val(paramDUDataObj.projects);
    $("#inp-update-designunit-address").val(paramDUDataObj.address);
    $("#inp-update-designunit-phone").val(paramDUDataObj.phone);
    $("#inp-update-designunit-phone2").val(paramDUDataObj.phone2);
    $("#inp-update-designunit-fax").val(paramDUDataObj.fax);
    $("#inp-update-designunit-email").val(paramDUDataObj.email);
    $("#inp-update-designunit-website").val(paramDUDataObj.website);
    $("#inp-update-designunit-note").val(paramDUDataObj.note);
}
//hàm thu thập update data
function getUpdateDUData(paramDUDataObj) {
    paramDUDataObj.name = $("#inp-update-designunit-name").val().trim();
    paramDUDataObj.description = $("#inp-update-designunit-description").val().trim();
    paramDUDataObj.projects = $("#inp-update-designunit-projects").val().trim();
    paramDUDataObj.address = $("#inp-update-designunit-address").val().trim();
    paramDUDataObj.phone = $("#inp-update-designunit-phone").val().trim();
    paramDUDataObj.phone2 = $("#inp-update-designunit-phone2").val().trim();
    paramDUDataObj.fax = $("#inp-update-designunit-fax").val().trim();
    paramDUDataObj.email = $("#inp-update-designunit-email").val().trim();
    paramDUDataObj.website = $("#inp-update-designunit-website").val().trim();
    paramDUDataObj.note = $("#inp-update-designunit-note").val().trim();
}
//hàm xử lý front-end khi update data thành công
function handleUpdateDUSuccess() {
    alert("Cập nhật thành công!");
    getAllDesignUnits();
    $("#update-designunit-modal").modal("hide");
}
//hàm xử lý front-end khi delete thành công
function handleDeleteDUSuccess() {
    alert("Xóa thành công!");
    getAllDesignUnits();
    $("#delete-designunit-modal").modal("hide");
}