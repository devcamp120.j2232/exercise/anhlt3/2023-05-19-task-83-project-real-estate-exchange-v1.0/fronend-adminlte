"use strict";
/*** CREATE_DATE 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/real-estates/";
// biến toàn cục để lưu trữ id đang dc update or delete. Mặc định = 0
var gRealEstateId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gRE_COLS = ["id",
    "province_id",
    "districtId",
    "wardId",
    "streetId",
    "type",
    "request",
    "customerId",
    "price",
    "createDate",
    "acreage",
    "totalFloors",
    "floorNumber",
    "width",
    "length",
    "photo",
    "priceTime",
    "action"
];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gID_COL = 0;
const gPROVINCE_COL = 1;
const gDISTRICT_COL = 2;
const gWARD_COL = 3;
const gSTREET_COL = 4;
const gTYPE_COL = 5;
const gREQUEST_COL = 6;
const gCUSTOMER_COL = 7;
const gPRICE_COL = 8;
const gCREATE_DATE_COL = 9;
const gACREAGE_COL = 10;
const gTOTAL_FLOOR_COL = 11;
const gFLOOR_NUMBER_COL = 12;
const gWIDTH_COL = 13;
const gLENGTH_COL = 14;
const gPHOTO_COL = 15;
const gPRICE_TIME_COL = 16;
const gACTION_COL = 17;

//khai báo DataTable & mapping columns
var gRealEstateTable = $("#realestate-table").DataTable({
    columns: [
        { data: gRE_COLS[gID_COL] },
        { data: gRE_COLS[gPROVINCE_COL] },
        { data: gRE_COLS[gDISTRICT_COL] },
        { data: gRE_COLS[gWARD_COL] },
        { data: gRE_COLS[gSTREET_COL] },
        { data: gRE_COLS[gTYPE_COL] },
        { data: gRE_COLS[gREQUEST_COL] },
        { data: gRE_COLS[gCUSTOMER_COL] },
        { data: gRE_COLS[gPRICE_COL] },
        { data: gRE_COLS[gCREATE_DATE_COL] },
        { data: gRE_COLS[gACREAGE_COL] },
        { data: gRE_COLS[gTOTAL_FLOOR_COL] },
        { data: gRE_COLS[gFLOOR_NUMBER_COL] },
        { data: gRE_COLS[gWIDTH_COL] },
        { data: gRE_COLS[gLENGTH_COL] },
        { data: gRE_COLS[gPHOTO_COL] },
        { data: gRE_COLS[gPRICE_TIME_COL] },
        { data: gRE_COLS[gACTION_COL] }
    ],
    columnDefs: [
        {
            targets: gACTION_COL,
            defaultContent: `
            <i class="fas fa-edit edit-realestate" style="cursor:pointer;color:blue;"></i>
            <i class="fas fa-trash delete-realestate" style="cursor:pointer;color:red;"></i>
          `
        }
    ]
});
/*** CREATE_DATE 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
});
//2 - C: gán sự kiện Create-thêm mới
$("#btn-add-new-realestate").on("click", function () {
    onBtnAddNewRealEstateClick();
});
//3 - U: gán sự kiện update-sửa
$("#realestate-table").on("click", ".edit-realestate", function () {
    onBtnEditRealEstateClick(this);
});
//4 - D: gán sự kiện delete-xóa
$("#realestate-table").on("click", ".delete-realestate", function () {
    onBtnDeleteRealEstateClick(this);
});

//gán sự kiện cho nút Create(trên modal)
$("#btn-create-realestate").on("click", function () {
    onBtnCreateRealEstateClick();
});
//gán sự kiện cho nút Update(trên modal)
$("#btn-update-realestate").on("click", function () {
    onBtnUpdateRealEstateClick();
});
//gán sự kiện cho nút Delete(trên modal)
$("#btn-confirm-delete-realestate").on("click", function () {
    onBtnDeleteRealEstateConfirmClick();
});
/*** CREATE_DATE 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    //1 - R: read/load to table
    getAllRealEstates(0, 10);
}
//hàm xử lý sự kiện nút Thêm mới
function onBtnAddNewRealEstateClick() {
    //hiển thị modal trắng lên
    $("#create-realestate-modal").modal("show");
}
//hàm xử lý sự kiện Update từ table
function onBtnEditRealEstateClick(paramEdit) {
    //lưu thông tin Id đang dc edit vào biến toàn cục
    gRealEstateId = getRealEstateIdFromButton(paramEdit);
    console.log("Id của obj tương ứng = " + gRealEstateId);
    // load dữ liệu vào các trường dữ liệu trong modal
    getRealEstateDataByRealEstateId(gRealEstateId);
}
//hàm xử lý sự kiện Delete từ table
function onBtnDeleteRealEstateClick(paramDelete) {
    //lưu thông tin Id đang dc xóa vào biến toàn cục
    gRealEstateId = getRealEstateIdFromButton(paramDelete);
    console.log("Id của obj tương ứng = " + gRealEstateId);
    $("#delete-realestate-modal").modal("show");
}

//hàm xử lý sự kiện nút Insert(trên modal)
function onBtnCreateRealEstateClick() {
    //khai báo obj chứa data
    var vRequestDataObj = {
        address: "",
        photo: "",
        provinceId: "",
        districtId: "",
        wardId: "",
        streetId: "",
        projectId: "",
        title: "",
        type: "",
        request: "",
        customerId: "",
        price: "",
        priceMin: "",
        priceTime: "",
        apartmentCode: "",
        wallArea: "",
        bedroom: "",
        balcony: "",
        landscapeView: "",
        apartmentLocation: "",
        apartmentType: "",
        furnitureType: "",
        priceRent: "",
        returnRate: "",
        acreage: "",
        direction: "",
        totalFloors: "",
        floorNumber: "",
        bath: "",
        legalDocument: "",
        description: "",
        width: "",
        length: "",
        isStreetHouse: "",
        isFBSO: "",
        viewNumber: "",
        shape: "",
        distanceToFacade: "",
        adjacentFacadeNumber: "",
        adjacentRoad: "",
        CTXDPrice: "",
        CTXDValue: "",
        alleyMinWidth: "",
        adjacentAlleyMinWidth: "",
        factor: "",
        structure: "",
        DTSXD: "",
        CLCL: "",
        lat: "",
        lng: ""
    }
    //b1: get data
    getInsertRealEstateData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateRealEstateData(vRequestDataObj);
    //ghi ra console value trả ra của hàm validate
    console.log(vIsValid);
    if (vIsValid) {
        //b3: insert data
        $.ajax({
            url: gBASE_URL,
            type: "POST",
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleInsertRealEstateSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

//hàm xử lý sự kiện nút Update(trên modal)
function onBtnUpdateRealEstateClick() {
    //khai báo obj chứa data
    var vRequestDataObj = {
        address: "",
        photo: "",
        provinceId: "",
        districtId: "",
        wardId: "",
        streetId: "",
        projectId: "",
        title: "",
        type: "",
        request: "",
        customerId: "",
        price: "",
        priceMin: "",
        priceTime: "",
        apartmentCode: "",
        wallArea: "",
        bedroom: "",
        balcony: "",
        landscapeView: "",
        apartmentLocation: "",
        apartmentType: "",
        furnitureType: "",
        priceRent: "",
        returnRate: "",
        acreage: "",
        direction: "",
        totalFloors: "",
        floorNumber: "",
        bath: "",
        legalDocument: "",
        description: "",
        width: "",
        length: "",
        isStreetHouse: "",
        isFBSO: "",
        viewNumber: "",
        shape: "",
        distanceToFacade: "",
        adjacentFacadeNumber: "",
        adjacentRoad: "",
        CTXDPrice: "",
        CTXDValue: "",
        alleyMinWidth: "",
        adjacentAlleyMinWidth: "",
        factor: "",
        structure: "",
        DTSXD: "",
        CLCL: "",
        lat: "",
        lng: ""
    }
    //b1: get data
    getUpdateRealEstateData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateRealEstateData(vRequestDataObj);
    if (vIsValid) {
        //b3: update data
        $.ajax({
            url: gBASE_URL + gRealEstateId,
            type: "PUT",
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleUpdateRealEstateSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

// hàm xử lý sự kiện delete trên modal
function onBtnDeleteRealEstateConfirmClick() {
    $.ajax({
        url: gBASE_URL + gRealEstateId,
        type: "DELETE",
        contentType: "application/json;charset-UTF-8",
        success: function (paramRes) {
            // B4: xử lý front-end
            handleDeleteRealEstateSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
/*** CREATE_DATE 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to DataTable
function loadDataToTable(paramRealEstate) {
    gRealEstateTable.clear();
    gRealEstateTable.rows.add(paramRealEstate);
    gRealEstateTable.draw();
}
//hàm gọi api để lấy all list
function getAllRealEstates(page, size) {
    $.ajax({
        url: gBASE_URL,
        type: "GET",
        data: { page: page, size: size },
        success: function (paramRealEstate) {
            //ghi response ra console
            console.log(paramRealEstate);
            loadDataToTable(paramRealEstate.content);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
//hàm dựa vào button detail (edit or delete) xác định dc id obj
function getRealEstateIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    var vRealEstateRowData = gRealEstateTable.row(vTableRow).data();
    return vRealEstateRowData.id;
}

//hàm thu thập insert data
function getInsertRealEstateData(paramRealEstateObj) {
    paramRealEstateObj.address = $("#inp-create-realestate-address").val().trim();
    var vPhotoInput = $("#inp-create-realestate-photo");
    var vFileName = vPhotoInput.val().split('\\').pop().split('/').pop();
    paramRealEstateObj.photo = vFileName;
    paramRealEstateObj.provinceId = $("#inp-create-realestate-provinceId").val().trim();
    paramRealEstateObj.districtId = $("#inp-create-realestate-districtId").val().trim();
    paramRealEstateObj.wardId = $("#inp-create-realestate-wardId").val().trim();
    paramRealEstateObj.streetId = $("#inp-create-realestate-streetId").val().trim();
    paramRealEstateObj.projectId = $("#inp-create-realestate-projectId").val().trim();
    paramRealEstateObj.title = $("#inp-create-realestate-title").val().trim();
    paramRealEstateObj.type = $("#inp-create-realestate-type").val().trim();
    paramRealEstateObj.request = $("#inp-create-realestate-request").val().trim();
    paramRealEstateObj.customerId = $("#inp-create-realestate-customerId").val().trim();
    paramRealEstateObj.price = $("#inp-create-realestate-price").val().trim();
    paramRealEstateObj.priceMin = $("#inp-create-realestate-priceMin").val().trim();
    paramRealEstateObj.priceTime = $("#inp-create-realestate-priceTime").val().trim();
    paramRealEstateObj.apartmentCode = $("#inp-create-realestate-apartmentCode").val().trim();
    paramRealEstateObj.wallArea = $("#inp-create-realestate-wallArea").val().trim();
    paramRealEstateObj.bedroom = $("#inp-create-realestate-bedroom").val().trim();
    paramRealEstateObj.balcony = $("#inp-create-realestate-balcony").val().trim();
    paramRealEstateObj.landscapeView = $("#inp-create-realestate-landscapeView").val().trim();
    paramRealEstateObj.apartmentLocation = $("#inp-create-realestate-apartmentLocation").val().trim();
    paramRealEstateObj.apartmentType = $("#inp-create-realestate-apartmentType").val().trim();
    paramRealEstateObj.furnitureType = $("#inp-create-realestate-furnitureType").val().trim();
    paramRealEstateObj.priceRent = $("#inp-create-realestate-priceRent").val().trim();
    paramRealEstateObj.returnRate = $("#inp-create-realestate-returnRate").val().trim();
    paramRealEstateObj.acreage = $("#inp-create-realestate-acreage").val().trim();
    paramRealEstateObj.direction = $("#inp-create-realestate-direction").val().trim();
    paramRealEstateObj.totalFloors = $("#inp-create-realestate-totalFloors").val().trim();
    paramRealEstateObj.floorNumber = $("#inp-create-realestate-floorNumber").val().trim();
    paramRealEstateObj.bath = $("#inp-create-realestate-bath").val().trim();
    paramRealEstateObj.legalDocument = $("#inp-create-realestate-legalDocument").val().trim();
    paramRealEstateObj.description = $("#inp-create-realestate-description").val().trim();
    paramRealEstateObj.width = $("#inp-create-realestate-width").val().trim();
    paramRealEstateObj.length = $("#inp-create-realestate-length").val().trim();
    paramRealEstateObj.isStreetHouse = $("#inp-create-realestate-isStreetHouse").val().trim();
    paramRealEstateObj.isFBSO = $("#inp-create-realestate-isFBSO").val().trim();
    paramRealEstateObj.viewNumber = $("#inp-create-realestate-viewNumber").val().trim();
    paramRealEstateObj.shape = $("#inp-create-realestate-shape").val().trim();
    paramRealEstateObj.distanceToFacade = $("#inp-create-realestate-distanceToFacade").val().trim();
    paramRealEstateObj.adjacentFacadeNumber = $("#inp-create-realestate-adjacentFacadeNumber").val().trim();
    paramRealEstateObj.adjacentRoad = $("#inp-create-realestate-adjacentRoad").val().trim();
    paramRealEstateObj.CTXDPrice = $("#inp-create-realestate-CTXDPrice").val().trim();
    paramRealEstateObj.CTXDValue = $("#inp-create-realestate-CTXDValue").val().trim();
    paramRealEstateObj.alleyMinWidth = $("#inp-create-realestate-alleyMinWidth").val().trim();
    paramRealEstateObj.adjacentAlleyMinWidth = $("#inp-create-realestate-adjacentAlleyMinWidth").val().trim();
    paramRealEstateObj.factor = $("#inp-create-realestate-factor").val().trim();
    paramRealEstateObj.structure = $("#inp-create-realestate-structure").val().trim();
    paramRealEstateObj.DTSXD = $("#inp-create-realestate-DTSXD").val().trim();
    paramRealEstateObj.CLCL = $("#inp-create-realestate-CLCL").val().trim();
    paramRealEstateObj.lat = $("#inp-create-realestate-lat").val().trim();
    paramRealEstateObj.lng = $("#inp-create-realestate-lng").val().trim();
}
//hàm validate data
function validateRealEstateData(paramRealEstateObj) {
    if (paramRealEstateObj.address === "") {
        alert("Chưa nhập địa chỉ!");
        return false;
    }

    return true;
}
//hàm xử lý front-end khi thêm obj thành công
function handleInsertRealEstateSuccess() {
    alert("Thêm mới thành công!");
    getAllRealEstates();
    resetCreateRealEstateForm();
    $("#create-realestate-modal").modal("hide");
}
//hàm xóa trắng form create
function resetCreateRealEstateForm() {
    $("#inp-create-realestate-address").val("");
    $("#inp-create-realestate-photo").val("");
    $("#inp-create-realestate-provinceId").val("");
    $("#inp-create-realestate-districtId").val("");
    $("#inp-create-realestate-wardId").val("");
    $("#inp-create-realestate-streetId").val("");
    $("#inp-create-realestate-projectId").val("");
    $("#inp-create-realestate-title").val("");
    $("#inp-create-realestate-type").val("");
    $("#inp-create-realestate-request").val("");
    $("#inp-create-realestate-customerId").val("");
    $("#inp-create-realestate-price").val("");
    $("#inp-create-realestate-priceMin").val("");
    $("#inp-create-realestate-priceTime").val("");
    $("#inp-create-realestate-apartmentCode").val("");
    $("#inp-create-realestate-wallArea").val("");
    $("#inp-create-realestate-bedroom").val("");
    $("#inp-create-realestate-balcony").val("");
    $("#inp-create-realestate-landscapeView").val("");
    $("#inp-create-realestate-apartmentLocation").val("");
    $("#inp-create-realestate-apartmentType").val("");
    $("#inp-create-realestate-furnitureType").val("");
    $("#inp-create-realestate-priceRent").val("");
    $("#inp-create-realestate-returnRate").val("");
    $("#inp-create-realestate-acreage").val("");
    $("#inp-create-realestate-direction").val("");
    $("#inp-create-realestate-totalFloors").val("");
    $("#inp-create-realestate-floorNumber").val("");
    $("#inp-create-realestate-bath").val("");
    $("#inp-create-realestate-legalDocument").val("");
    $("#inp-create-realestate-description").val("");
    $("#inp-create-realestate-width").val("");
    $("#inp-create-realestate-length").val("");
    $("#inp-create-realestate-isStreetHouse").val("");
    $("#inp-create-realestate-isFBSO").val("");
    $("#inp-create-realestate-viewNumber").val("");
    $("#inp-create-realestate-shape").val("");
    $("#inp-create-realestate-distanceToFacade").val("");
    $("#inp-create-realestate-adjacentFacadeNumber").val("");
    $("#inp-create-realestate-adjacentRoad").val("");
    $("#inp-create-realestate-CTXDPrice").val("");
    $("#inp-create-realestate-CTXDValue").val("");
    $("#inp-create-realestate-alleyMinWidth").val("");
    $("#inp-create-realestate-adjacentAlleyMinWidth").val("");
    $("#inp-create-realestate-factor").val("");
    $("#inp-create-realestate-structure").val("");
    $("#inp-create-realestate-DTSXD").val("");
    $("#inp-create-realestate-CLCL").val("");
    $("#inp-create-realestate-lat").val("");
    $("#inp-create-realestate-lng").val("");
}

//hàm load data lên update modal form
function getRealEstateDataByRealEstateId(paramRealEstateId) {
    $.ajax({
        url: gBASE_URL + paramRealEstateId,
        type: "GET",
        success: function (paramRealEstateId) {
            showRealEstateDataToUpdateModal(paramRealEstateId);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
    // hiển thị modal lên
    $("#update-realestate-modal").modal("show");
}
//hàm show data lên update modal form
function showRealEstateDataToUpdateModal(paramRealEstateDataObj) {
    $("#inp-update-realestate-id").val(paramRealEstateDataObj.id);
    $("#inp-update-realestate-address").val(paramRealEstateDataObj.address);
    // Hiển thị tên file
    var vFileName = paramRealEstateDataObj.photo.split('/').pop();
    $("#file-label").text(vFileName);
    $("#inp-update-realestate-provinceId").val(paramRealEstateDataObj.provinceId);
    $("#inp-update-realestate-districtId").val(paramRealEstateDataObj.districtId);
    $("#inp-update-realestate-wardId").val(paramRealEstateDataObj.wardId);
    $("#inp-update-realestate-streetId").val(paramRealEstateDataObj.streetId);
    $("#inp-update-realestate-projectId").val(paramRealEstateDataObj.projectId);
    $("#inp-update-realestate-title").val(paramRealEstateDataObj.title);
    $("#inp-update-realestate-type").val(paramRealEstateDataObj.type);
    $("#inp-update-realestate-request").val(paramRealEstateDataObj.request);
    $("#inp-update-realestate-customerId").val(paramRealEstateDataObj.customerId);
    $("#inp-update-realestate-price").val(paramRealEstateDataObj.price);
    $("#inp-update-realestate-priceMin").val(paramRealEstateDataObj.priceMin);
    $("#inp-update-realestate-priceTime").val(paramRealEstateDataObj.priceTime);
    $("#inp-update-realestate-apartmentCode").val(paramRealEstateDataObj.apartmentCode);
    $("#inp-update-realestate-wallArea").val(paramRealEstateDataObj.wallArea);
    $("#inp-update-realestate-bedroom").val(paramRealEstateDataObj.bedroom);
    $("#inp-update-realestate-balcony").val(paramRealEstateDataObj.balcony);
    $("#inp-update-realestate-landscapeView").val(paramRealEstateDataObj.landscapeView);
    $("#inp-update-realestate-apartmentLocation").val(paramRealEstateDataObj.apartmentLocation);
    $("#inp-update-realestate-apartmentType").val(paramRealEstateDataObj.apartmentType);
    $("#inp-update-realestate-furnitureType").val(paramRealEstateDataObj.furnitureType);
    $("#inp-update-realestate-priceRent").val(paramRealEstateDataObj.priceRent);
    $("#inp-update-realestate-returnRate").val(paramRealEstateDataObj.returnRate);
    $("#inp-update-realestate-acreage").val(paramRealEstateDataObj.acreage);
    $("#inp-update-realestate-direction").val(paramRealEstateDataObj.direction);
    $("#inp-update-realestate-totalFloors").val(paramRealEstateDataObj.totalFloors);
    $("#inp-update-realestate-floorNumber").val(paramRealEstateDataObj.floorNumber);
    $("#inp-update-realestate-bath").val(paramRealEstateDataObj.bath);
    $("#inp-update-realestate-legalDocument").val(paramRealEstateDataObj.legalDocument);
    $("#inp-update-realestate-description").val(paramRealEstateDataObj.description);
    $("#inp-update-realestate-width").val(paramRealEstateDataObj.width);
    $("#inp-update-realestate-length").val(paramRealEstateDataObj.length);
    $("#inp-update-realestate-isStreetHouse").val(paramRealEstateDataObj.isStreetHouse);
    $("#inp-update-realestate-isFBSO").val(paramRealEstateDataObj.isFBSO);
    $("#inp-update-realestate-viewNumber").val(paramRealEstateDataObj.viewNumber);
    $("#inp-update-realestate-shape").val(paramRealEstateDataObj.shape);
    $("#inp-update-realestate-distanceToFacade").val(paramRealEstateDataObj.distanceToFacade);
    $("#inp-update-realestate-adjacentFacadeNumber").val(paramRealEstateDataObj.adjacentFacadeNumber);
    $("#inp-update-realestate-adjacentRoad").val(paramRealEstateDataObj.adjacentRoad);
    $("#inp-update-realestate-CTXDPrice").val(paramRealEstateDataObj.CTXDPrice);
    $("#inp-update-realestate-CTXDValue").val(paramRealEstateDataObj.CTXDValue);
    $("#inp-update-realestate-alleyMinWidth").val(paramRealEstateDataObj.alleyMinWidth);
    $("#inp-update-realestate-adjacentAlleyMinWidth").val(paramRealEstateDataObj.adjacentAlleyMinWidth);
    $("#inp-update-realestate-factor").val(paramRealEstateDataObj.factor);
    $("#inp-update-realestate-structure").val(paramRealEstateDataObj.structure);
    $("#inp-update-realestate-DTSXD").val(paramRealEstateDataObj.DTSXD);
    $("#inp-update-realestate-CLCL").val(paramRealEstateDataObj.CLCL);
    $("#inp-update-realestate-lat").val(paramRealEstateDataObj.lat);
    $("#inp-update-realestate-lng").val(paramRealEstateDataObj.lng);
}
//hàm thu thập update data
function getUpdateRealEstateData(paramRealEstateDataObj) {
    paramRealEstateDataObj.address = $("#inp-update-realestate-address").val().trim();
    var vPhotoInput = $("#inp-update-realestate-photo");
    var vFileName = vPhotoInput.val().split('\\').pop().split('/').pop();
    paramRealEstateDataObj.photo = vFileName;
    paramRealEstateDataObj.provinceId = $("#inp-update-realestate-provinceId").val().trim();
    paramRealEstateDataObj.districtId = $("#inp-update-realestate-districtId").val().trim();
    paramRealEstateDataObj.wardId = $("#inp-update-realestate-wardId").val().trim();
    paramRealEstateDataObj.streetId = $("#inp-update-realestate-streetId").val().trim();
    paramRealEstateDataObj.projectId = $("#inp-update-realestate-projectId").val().trim();
    paramRealEstateDataObj.title = $("#inp-update-realestatetitle-").val().trim();
    paramRealEstateDataObj.type = $("#inp-update-realestate-type").val().trim();
    paramRealEstateDataObj.request = $("#inp-update-realestate-request").val().trim();
    paramRealEstateDataObj.customerId = $("#inp-update-realestate-customerId").val().trim();
    paramRealEstateDataObj.price = $("#inp-update-realestate-price").val().trim();
    paramRealEstateDataObj.priceMin = $("#inp-update-realestate-priceMin").val().trim();
    paramRealEstateDataObj.priceTime = $("#inp-update-realestate-priceTime").val().trim();
    paramRealEstateDataObj.apartmentCode = $("#inp-update-realestate-apartmentCode").val().trim();
    paramRealEstateDataObj.wallArea = $("#inp-update-realestate-wallArea").val().trim();
    paramRealEstateDataObj.bedroom = $("#inp-update-realestate-bedroom").val().trim();
    paramRealEstateDataObj.balcony = $("#inp-update-realestate-balcony").val().trim();
    paramRealEstateDataObj.landscapeView = $("#inp-update-realestate-landscapeView").val().trim();
    paramRealEstateDataObj.apartmentLocation = $("#inp-update-realestate-apartmentLocation").val().trim();
    paramRealEstateDataObj.apartmentType = $("#inp-update-realestate-apartmentType").val().trim();
    paramRealEstateDataObj.furnitureType = $("#inp-update-realestate-furnitureType").val().trim();
    paramRealEstateDataObj.priceRent = $("#inp-update-realestate-priceRent").val().trim();
    paramRealEstateDataObj.returnRate = $("#inp-update-realestate-returnRate").val().trim();
    paramRealEstateDataObj.acreage = $("#inp-update-realestate-acreage").val().trim();
    paramRealEstateDataObj.direction = $("#inp-update-realestate-direction").val().trim();
    paramRealEstateDataObj.totalFloors = $("#inp-update-realestate-totalFloors").val().trim();
    paramRealEstateDataObj.floorNumber = $("#inp-update-realestate-floorNumber").val().trim();
    paramRealEstateDataObj.bath = $("#inp-update-realestate-bath").val().trim();
    paramRealEstateDataObj.legalDocument = $("#inp-update-realestate-legalDocument").val().trim();
    paramRealEstateDataObj.description = $("#inp-update-realestate-description").val().trim();
    paramRealEstateDataObj.width = $("#inp-update-realestate-width").val().trim();
    paramRealEstateDataObj.length = $("#inp-update-realestate-length").val().trim();
    paramRealEstateDataObj.isStreetHouse = $("#inp-update-realestate-isStreetHouse").val().trim();
    paramRealEstateDataObj.isFBSO = $("#inp-update-realestate-isFBSO").val().trim();
    paramRealEstateDataObj.viewNumber = $("#inp-update-realestate-viewNumber").val().trim();
    paramRealEstateDataObj.shape = $("#inp-update-realestate-shape").val().trim();
    paramRealEstateDataObj.distanceToFacade = $("#inp-update-realestate-distanceToFacade").val().trim();
    paramRealEstateDataObj.adjacentFacadeNumber = $("#inp-update-realestate-adjacentFacadeNumber").val().trim();
    paramRealEstateDataObj.adjacentRoad = $("#inp-update-realestate-adjacentRoad").val().trim();
    paramRealEstateDataObj.CTXDPrice = $("#inp-update-realestate-CTXDPrice").val().trim();
    paramRealEstateDataObj.CTXDValue = $("#inp-update-realestate-CTXDValue").val().trim();
    paramRealEstateDataObj.alleyMinWidth = $("#inp-update-realestate-alleyMinWidth").val().trim();
    paramRealEstateDataObj.adjacentAlleyMinWidth = $("#inp-update-realestate-adjacentAlleyMinWidth").val().trim();
    paramRealEstateDataObj.factor = $("#inp-update-realestate-factor").val().trim();
    paramRealEstateDataObj.structure = $("#inp-update-realestate-structure").val().trim();
    paramRealEstateDataObj.DTSXD = $("#inp-update-realestate-DTSXD").val().trim();
    paramRealEstateDataObj.CLCL = $("#inp-update-realestate-CLCL").val().trim();
    paramRealEstateDataObj.lat = $("#inp-update-realestate-lat").val().trim();
    paramRealEstateDataObj.lng = $("#inp-update-realestate-lng").val().trim();
}
//hàm xử lý front-end khi update data thành công
function handleUpdateRealEstateSuccess() {
    alert("Cập nhật thành công!");
    getAllRealEstates();
    $("#update-realestate-modal").modal("hide");
}
//hàm xử lý front-end khi delete thành công
function handleDeleteRealEstateSuccess() {
    alert("Xóa thành công!");
    getAllRealEstates();
    $("#delete-realestate-modal").modal("hide");
}