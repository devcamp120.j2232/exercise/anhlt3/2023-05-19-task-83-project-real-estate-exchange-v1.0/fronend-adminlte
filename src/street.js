"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/streets/";
var gStreetId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gWARD_COLS = ["id", "name", "prefix", "provinceId", "districtId", "action"];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gID_COL = 0;
const gNAME_COL = 1;
const gPREFIX_COL = 2;
const gPROVINCE_COL = 3;
const gDISTRICT_COL = 4;
const gACTION_COL = 5;

//khai báo DataTable & mapping columns
var gStreetTable = $("#street-table").DataTable({
  serverSide: true,
  paging: true,
  ajax: {
    url: gBASE_URL,
    type: "GET",
    dataType: "json",
    data: function (params) {
      return {
        page: params.start / params.length,
        size: params.length
      };
    },
    dataSrc: function (response) {
      return response.content;
    }
  },
  columns: [
    { data: gWARD_COLS[gID_COL] },
    { data: gWARD_COLS[gNAME_COL] },
    { data: gWARD_COLS[gPREFIX_COL] },
    {
      data: gWARD_COLS[gPROVINCE_COL],
      render: function (data) {
        return data.name;
      }
    },
    {
      data: gWARD_COLS[gDISTRICT_COL],
      render: function (data) {
        return data.name;
      }
    },
    { data: gWARD_COLS[gACTION_COL] }
  ],
  columnDefs: [
    {
      targets: gACTION_COL,
      render: function () {
        return `
          <i class="fas fa-edit edit-street" style="cursor:pointer;color:blue;"></i>
          <i class="fas fa-trash delete-street" style="cursor:pointer;color:red;"></i>
        `;
      }
    },
  ],
  drawCallback: function () {
    var info = $("#street-table_info");
    var pageInfo = gStreetTable.page.info();
    var start = pageInfo.start + 1;
    var end = pageInfo.start + gStreetTable.rows().count();
    var total = pageInfo.recordsTotal;

    // info.html(`Showing ${start} to ${end} of ${total} entries`);
    info.html(`Showing ${start} to ${end} entries`);
  }
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  onPageLoading();
});
//2 - C: gán sự kiện Create-thêm mới
$("#btn-add-new-street").on("click", function () {
  onBtnAddNewStreetClick();
});
//3 - U: gán sự kiện update-sửa 1
$("#street-table").on("click", ".edit-street", function () {
  onBtnEditStreetClick(this);
});
//4 - D: gán sự kiện delete-xóa 1
$("#street-table").on("click", ".delete-street", function () {
  onBtnDeleteStreetClick(this);
});

//gán sự kiện cho nút Create(trên modal)
$("#btn-create-street").on("click", function () {
  onBtnCreateStreetClick();
});
//gán sự kiện cho nút Update(trên modal)
$("#btn-update-street").on("click", function () {
  onBtnUpdateStreetClick();
});
//gán sự kiện cho nút Delete(trên modal)
$("#btn-confirm-delete-street").on("click", function () {
  onBtnDeleteStreetConfirmClick();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  //1 - R: read/load to table
  getAllStreets();
}
//hàm xử lý sự kiện nút Thêm mới
function onBtnAddNewStreetClick() {
  //hiển thị modal trắng lên
  $("#create-street-modal").modal("show");
}
//hàm xử lý sự kiện Update từ table
function onBtnEditStreetClick(paramEdit) {
  //lưu thông tin Id đang dc edit vào biến toàn cục
  gStreetId = getStreetIdFromButton(paramEdit);
  console.log("Id của obj tương ứng = " + gStreetId);
  // load dữ liệu vào các trường dữ liệu trong modal
  getStreetDataByStreetId(gStreetId);
}
//hàm xử lý sự kiện Delete từ table
function onBtnDeleteStreetClick(paramDelete) {
  //lưu thông tin Id đang dc xóa vào biến toàn cục
  gStreetId = getStreetIdFromButton(paramDelete);
  console.log("Id của obj tương ứng = " + gStreetId);
  $("#delete-street-modal").modal("show");
}

//hàm xử lý sự kiện nút Insert(trên modal)
function onBtnCreateStreetClick() {
  //khai báo obj chứa data
  var vRequestDataObj = {
    name: "",
    prefix: "",
    provinceId: {
      id: ""
    },
    districtId: {
      id: ""
    }
  }
  //b1: get data
  getInsertStreetData(vRequestDataObj);
  //b2: validate data
  var vIsValid = validateStreetData(vRequestDataObj);
  //ghi ra console value trả ra
  console.log(vIsValid);
  if (vIsValid) {
    vRequestDataObj.provinceId = {
      id: parseInt(vRequestDataObj.provinceId)
    };
    vRequestDataObj.districtId = {
      id: parseInt(vRequestDataObj.districtId)
    };
    //b3: insert
    $.ajax({
      url: gBASE_URL,
      type: "POST",
      contentType: "application/json;charset-UTF-8",
      data: JSON.stringify(vRequestDataObj),
      success: function (paramRes) {
        console.log(paramRes);
        //b4: xử lý front-end
        handleInsertStreetSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

//hàm xử lý sự kiện nút Update(trên modal)
function onBtnUpdateStreetClick() {
  //khai báo obj chứa data
  var vRequestDataObj = {
    name: "",
    prefix: "",
    provinceId: {
      id: ""
    },
    districtId: {
      id: ""
    }
  }
  //b1: get data
  getUpdateStreetData(vRequestDataObj);
  //b2: validate data
  var vIsValid = validateStreetData(vRequestDataObj);
  if (vIsValid) {
    vRequestDataObj.provinceId = {
      id: parseInt(vRequestDataObj.provinceId)
    };
    vRequestDataObj.districtId = {
      id: parseInt(vRequestDataObj.districtId)
    };
    //b3: update
    $.ajax({
      url: gBASE_URL + gStreetId,
      type: "PUT",
      contentType: "application/json;charset-UTF-8",
      data: JSON.stringify(vRequestDataObj),
      success: function (paramRes) {
        console.log(paramRes);
        //b4: xử lý front-end
        handleUpdateStreetSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

// hàm xử lý sự kiện delete trên modal
function onBtnDeleteStreetConfirmClick() {
  $.ajax({
    url: gBASE_URL + gStreetId,
    type: "DELETE",
    contentType: "application/json;charset-UTF-8",
    success: function (paramRes) {
      // B4: xử lý front-end
      handleDeleteStreetSuccess();
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to DataTable
function loadDataToTable(paramStreet) {
  gStreetTable.clear();
  gStreetTable.rows.add(paramStreet);
  gStreetTable.draw();
}
//hàm gọi api để lấy all list
function getAllStreets() {
  const page = 0; // Trang hiện tại
  const size = 10; // Số mục trên mỗi trang

  const url = `${gBASE_URL}?page=${page}&size=${size}`;
  $.ajax({
    url: url,
    type: "GET",
    success: function (paramStreet) {
      //ghi response ra console
      console.log(paramStreet);
      loadDataToTable(paramStreet);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
//hàm dựa vào button detail (edit or delete) xác định dc id obj
function getStreetIdFromButton(paramButton) {
  var vTableRow = $(paramButton).parents("tr");
  var vStreetRowData = gStreetTable.row(vTableRow).data();
  return vStreetRowData.id;
}

//hàm thu thập insert data
function getInsertStreetData(paramObj) {
  paramObj.name = $("#inp-create-street-name").val().trim();
  paramObj.prefix = $("#select-create-prefix").val();
  paramObj.provinceId = $("#select-create-province").val();
  paramObj.districtId = $("#select-create-district").val();
}
//hàm validate data
function validateStreetData(paramObj) {
  if (paramObj.name === "") {
    alert("Chưa nhập tên!");
    return false;
  }
  return true;
}
//hàm xử lý front-end khi thêm obj thành công
function handleInsertStreetSuccess() {
  alert("Thêm mới thành công!");
  getAllStreets();
  resetCreateStreetForm();
  $("#create-street-modal").modal("hide");
}
//hàm xóa trắng form create
function resetCreateStreetForm() {
  $("#inp-create-street-name").val("");
  $("#select-create-prefix").val("");
  $("#select-create-province").val("");
  $("#select-create-district").val("");
}

//hàm load data lên update modal form
function getStreetDataByStreetId(paramId) {
  $.ajax({
    url: gBASE_URL + paramId,
    type: "GET",
    success: function (paramId) {
      showStreetDataToUpdateModal(paramId);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
  // hiển thị modal lên
  $("#update-street-modal").modal("show");
}
//hàm show data lên update modal form
function showStreetDataToUpdateModal(paramDataObj) {
  $("#inp-update-street-id").val(paramDataObj.id);
  $("#inp-update-street-name").val(paramDataObj.name);
  $("#select-update-prefix").val(paramDataObj.prefix);

  $("#select-update-province option").each(function () {
    // Kiểm tra giá trị option có khớp với provinceId hay không
    if ($(this).val() == paramDataObj.provinceId.id) {
      // Đặt selected cho tùy chọn khớp
      $(this).prop("selected", true);
    }
  });

  $("#select-update-district option").each(function () {
    if ($(this).val() == paramDataObj.districtId.id) {
      $(this).prop("selected", true);
    }
  });
}
//hàm thu thập update data
function getUpdateStreetData(paramDataObj) {
  paramDataObj.name = $("#inp-update-street-name").val().trim();
  paramDataObj.prefix = $("#select-update-prefix").val();
  paramDataObj.provinceId = $("#select-update-province").val();
  paramDataObj.districtId = $("#select-update-district").val();
}
//hàm xử lý front-end khi update data thành công
function handleUpdateStreetSuccess() {
  alert("Cập nhật thành công!");
  getAllStreets();
  $("#update-street-modal").modal("hide");
}
//hàm xử lý front-end khi delete thành công
function handleDeleteStreetSuccess() {
  alert("Xóa thành công!");
  getAllStreets();
  $("#delete-street-modal").modal("hide");
}