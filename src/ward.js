"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/wards/";
var gWardId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gWARD_COLS = ["id", "name", "prefix", "provinceId", "districtId", "action"];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gID_COL = 0;
const gNAME_COL = 1;
const gPREFIX_COL = 2;
const gPROVINCE_COL = 3;
const gDISTRICT_COL = 4;
const gACTION_COL = 5;

//khai báo DataTable & mapping columns
var gWardTable = $("#ward-table").DataTable({
  serverSide: true,
  paging: true,
  ajax: {
    url: gBASE_URL,
    type: "GET",
    dataType: "json",
    data: function (params) {
      return {
        page: params.start / params.length,
        size: params.length
      };
    },
    dataSrc: function (response) {
      return response.content; // Specify the data source property in the response
    }
  },
  columns: [
    { data: gWARD_COLS[gID_COL] },
    { data: gWARD_COLS[gNAME_COL] },
    { data: gWARD_COLS[gPREFIX_COL] },
    {
      data: gWARD_COLS[gPROVINCE_COL],
      render: function (data) {
        return data.name;
      }
    },
    {
      data: gWARD_COLS[gDISTRICT_COL],
      render: function (data) {
        return data.name;
      }
    },
    { data: gWARD_COLS[gACTION_COL] }
  ],
  columnDefs: [
    {
      targets: gACTION_COL,
      defaultContent: `
            <i class="fas fa-edit edit-ward" style="cursor:pointer;color:blue;"></i>
            <i class="fas fa-trash delete-ward" style="cursor:pointer;color:red;"></i>
          `
    }
  ]
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  onPageLoading();
});
//2 - C: gán sự kiện Create-thêm mới
$("#btn-add-new-ward").on("click", function () {
  onBtnAddNewWardClick();
});
//3 - U: gán sự kiện update-sửa 1
$("#ward-table").on("click", ".edit-ward", function () {
  onBtnEditWardClick(this);
});
//4 - D: gán sự kiện delete-xóa 1
$("#ward-table").on("click", ".delete-ward", function () {
  onBtnDeleteWardClick(this);
});

//gán sự kiện cho nút Create(trên modal)
$("#btn-create-ward").on("click", function () {
  onBtnCreateWardClick();
});
//gán sự kiện cho nút Update(trên modal)
$("#btn-update-ward").on("click", function () {
  onBtnUpdateWardClick();
});
//gán sự kiện cho nút Delete(trên modal)
$("#btn-confirm-delete-ward").on("click", function () {
  onBtnDeleteWardConfirmClick();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  //1 - R: read/load to table
  getAllWards();
}
//hàm xử lý sự kiện nút Thêm mới
function onBtnAddNewWardClick() {
  //hiển thị modal trắng lên
  $("#create-ward-modal").modal("show");
}
//hàm xử lý sự kiện Update từ table
function onBtnEditWardClick(paramEdit) {
  //lưu thông tin Id đang dc edit vào biến toàn cục
  gWardId = getWardIdFromButton(paramEdit);
  console.log("Id của obj tương ứng = " + gWardId);
  // load dữ liệu vào các trường dữ liệu trong modal
  getWardDataByWardId(gWardId);
}
//hàm xử lý sự kiện Delete từ table
function onBtnDeleteWardClick(paramDelete) {
  //lưu thông tin Id đang dc xóa vào biến toàn cục
  gWardId = getWardIdFromButton(paramDelete);
  console.log("Id của obj tương ứng = " + gWardId);
  $("#delete-ward-modal").modal("show");
}

//hàm xử lý sự kiện nút Insert(trên modal)
function onBtnCreateWardClick() {
  //khai báo obj chứa data
  var vRequestDataObj = {
    name: "",
    prefix: "",
    provinceId: {
      id: ""
    },
    districtId: {
      id: ""
    }
  }
  //b1: get data
  getInsertWardData(vRequestDataObj);
  //b2: validate data
  var vIsValid = validateWardData(vRequestDataObj);
  //ghi ra console value trả ra
  console.log(vIsValid);
  if (vIsValid) {
    vRequestDataObj.provinceId = {
      id: parseInt(vRequestDataObj.provinceId)
    };
    vRequestDataObj.districtId = {
      id: parseInt(vRequestDataObj.districtId)
    };
    //b3: insert
    $.ajax({
      url: gBASE_URL,
      type: "POST",
      contentType: "application/json;charset-UTF-8",
      data: JSON.stringify(vRequestDataObj),
      success: function (paramRes) {
        console.log(paramRes);
        //b4: xử lý front-end
        handleInsertWardSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

//hàm xử lý sự kiện nút Update(trên modal)
function onBtnUpdateWardClick() {
  //khai báo obj chứa data
  var vRequestDataObj = {
    name: "",
    prefix: "",
    provinceId: {
      id: ""
    },
    districtId: {
      id: ""
    }
  }
  //b1: get data
  getUpdateWardData(vRequestDataObj);
  //b2: validate data
  var vIsValid = validateWardData(vRequestDataObj);
  if (vIsValid) {
    vRequestDataObj.provinceId = {
      id: parseInt(vRequestDataObj.provinceId)
    };
    vRequestDataObj.districtId = {
      id: parseInt(vRequestDataObj.districtId)
    };
    //b3: update
    $.ajax({
      url: gBASE_URL + gWardId,
      type: "PUT",
      contentType: "application/json;charset-UTF-8",
      data: JSON.stringify(vRequestDataObj),
      success: function (paramRes) {
        console.log(paramRes);
        //b4: xử lý front-end
        handleUpdateWardSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

// hàm xử lý sự kiện delete trên modal
function onBtnDeleteWardConfirmClick() {
  $.ajax({
    url: gBASE_URL + gWardId,
    type: "DELETE",
    contentType: "application/json;charset-UTF-8",
    success: function (paramRes) {
      // B4: xử lý front-end
      handleDeleteWardSuccess();
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to DataTable
function loadDataToTable(paramWard) {
  gWardTable.clear();
  gWardTable.rows.add(paramWard);
  gWardTable.draw();
}
//hàm gọi api để lấy all list
function getAllWards() {
  const page = 0; // Trang hiện tại
  const size = 10; // Số mục trên mỗi trang

  const url = `${gBASE_URL}?page=${page}&size=${size}`;
  $.ajax({
    url: url,
    type: "GET",
    success: function (paramWard) {
      //ghi response ra console
      console.log(paramWard);
      loadDataToTable(paramWard);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
//hàm dựa vào button detail (edit or delete) xác định dc id obj
function getWardIdFromButton(paramButton) {
  var vTableRow = $(paramButton).parents("tr");
  var vWardRowData = gWardTable.row(vTableRow).data();
  return vWardRowData.id;
}

//hàm thu thập insert data
function getInsertWardData(paramObj) {
  paramObj.name = $("#inp-create-ward-name").val().trim();
  paramObj.prefix = $("#select-create-prefix").val();
  paramObj.provinceId = $("#select-create-province").val();
  paramObj.districtId = $("#select-create-district").val();
}
//hàm validate data
function validateWardData(paramObj) {
  if (paramObj.name === "") {
    alert("Chưa nhập tên!");
    return false;
  }
  return true;
}
//hàm xử lý front-end khi thêm obj thành công
function handleInsertWardSuccess() {
  alert("Thêm mới thành công!");
  getAllWards();
  resetCreateWardForm();
  $("#create-ward-modal").modal("hide");
}
//hàm xóa trắng form create
function resetCreateWardForm() {
  $("#inp-create-ward-name").val("");
  $("#select-create-prefix").val("");
  $("#select-create-province").val("");
  $("#select-create-district").val("");
}

//hàm load data lên update modal form
function getWardDataByWardId(paramId) {
  $.ajax({
    url: gBASE_URL + paramId,
    type: "GET",
    success: function (paramId) {
      showWardDataToUpdateModal(paramId);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
  // hiển thị modal lên
  $("#update-ward-modal").modal("show");
}
//hàm show data lên update modal form
function showWardDataToUpdateModal(paramDataObj) {
  $("#inp-update-ward-id").val(paramDataObj.id);
  $("#inp-update-ward-name").val(paramDataObj.name);
  $("#select-update-prefix").val(paramDataObj.prefix);

  $("#select-update-province option").each(function () {
    // Kiểm tra giá trị option có khớp với provinceId hay không
    if ($(this).val() == paramDataObj.provinceId.id) {
      // Đặt selected cho tùy chọn khớp
      $(this).prop("selected", true);
    }
  });

  $("#select-update-district option").each(function () {
    if ($(this).val() == paramDataObj.districtId.id) {
      $(this).prop("selected", true);
    }
  });
}
//hàm thu thập update data
function getUpdateWardData(paramDataObj) {
  paramDataObj.name = $("#inp-update-ward-name").val().trim();
  paramDataObj.prefix = $("#select-update-prefix").val();
  paramDataObj.provinceId = $("#select-update-province").val();
  paramDataObj.districtId = $("#select-update-district").val();
}
//hàm xử lý front-end khi update data thành công
function handleUpdateWardSuccess() {
  alert("Cập nhật thành công!");
  getAllWards();
  $("#update-ward-modal").modal("hide");
}
//hàm xử lý front-end khi delete thành công
function handleDeleteWardSuccess() {
  alert("Xóa thành công!");
  getAllWards();
  $("#delete-ward-modal").modal("hide");
}