"use strict";
/*** NUM_APARTMENT 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/projects/";
// biến toàn cục để lưu trữ id đang dc update or delete. Mặc định = 0
var gProjectId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gPROJECT_COLS = [ "id",
                        "name",
                        "provinceId",
                        "districtId",
                        "lat",
                        "lng",
                        "acreage",
                        "constructArea",
                        "numBlock",
                        "numApartment",
                        "investor",
                        "constructionContractor",
                        "designUnit",
                        "action"
                    ];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gID_COL = 0;
const gNAME_COL = 1;
const gPROVINCE_COL = 2;
const gDISTRICT_COL = 3;
const gLAT_COL = 4;
const gLNG_COL = 5;
const gACREAGE_COL = 6;
const gCONSTRUCT_AREA_COL = 7;
const gNUM_BLOCK_COL = 8;
const gNUM_APARTMENT_COL = 9;
const gINVESTOR_COL = 10;
const gCONSTUCTION_CONTRACTOR_COL = 11;
const gDESIGN_UNIT_COL = 12;
const gACTION_COL = 13;

//khai báo DataTable & mapping columns
var gProjectTable = $("#project-table").DataTable({
    columns: [
        { data: gPROJECT_COLS[gID_COL] },
        { data: gPROJECT_COLS[gNAME_COL] },
        { data: gPROJECT_COLS[gPROVINCE_COL] },
        { data: gPROJECT_COLS[gDISTRICT_COL] },
        { data: gPROJECT_COLS[gLAT_COL] },
        { data: gPROJECT_COLS[gLNG_COL] },
        { data: gPROJECT_COLS[gACREAGE_COL] },
        { data: gPROJECT_COLS[gCONSTRUCT_AREA_COL] },
        { data: gPROJECT_COLS[gNUM_BLOCK_COL] },
        { data: gPROJECT_COLS[gNUM_APARTMENT_COL] },
        { data: gPROJECT_COLS[gINVESTOR_COL] },
        { data: gPROJECT_COLS[gCONSTUCTION_CONTRACTOR_COL] },
        { data: gPROJECT_COLS[gDESIGN_UNIT_COL] },
        { data: gPROJECT_COLS[gACTION_COL] }
    ],
    columnDefs: [
        {
            targets: gACTION_COL,
            defaultContent: `
            <i class="fas fa-edit edit-project" style="cursor:pointer;color:blue;"></i>
            <i class="fas fa-trash delete-project" style="cursor:pointer;color:red;"></i>
          `
        }
    ]
});
/*** NUM_APARTMENT 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
});
//2 - C: gán sự kiện Create-thêm mới
$("#btn-add-new-project").on("click", function () {
    onBtnAddNewProjectClick();
});
//3 - U: gán sự kiện update-sửa
$("#project-table").on("click", ".edit-project", function () {
    onBtnEditProjectClick(this);
});
//4 - D: gán sự kiện delete-xóa
$("#project-table").on("click", ".delete-project", function () {
    onBtnDeleteProjectClick(this);
});

//gán sự kiện cho nút Create(trên modal)
$("#btn-create-project").on("click", function () {
    onBtnCreateProjectClick();
});
//gán sự kiện cho nút Update(trên modal)
$("#btn-update-project").on("click", function () {
    onBtnUpdateProjectClick();
});
//gán sự kiện cho nút Delete(trên modal)
$("#btn-confirm-delete-project").on("click", function () {
    onBtnDeleteProjectConfirmClick();
});
/*** NUM_APARTMENT 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    //1 - R: read/load to table
    getAllProjects();
}
//hàm xử lý sự kiện nút Thêm mới
function onBtnAddNewProjectClick() {
    //hiển thị modal trắng lên
    $("#create-project-modal").modal("show");
}
//hàm xử lý sự kiện Update từ table
function onBtnEditProjectClick(paramEdit) {
    //lưu thông tin Id đang dc edit vào biến toàn cục
    gProjectId = getProjectIdFromButton(paramEdit);
    console.log("Id của obj tương ứng = " + gProjectId);
    // load dữ liệu vào các trường dữ liệu trong modal
    getProjectDataByProjectId(gProjectId);
}
//hàm xử lý sự kiện Delete từ table
function onBtnDeleteProjectClick(paramDelete) {
    //lưu thông tin Id đang dc xóa vào biến toàn cục
    gProjectId = getProjectIdFromButton(paramDelete);
    console.log("Id của obj tương ứng = " + gProjectId);
    $("#delete-project-modal").modal("show");
}

//hàm xử lý sự kiện nút Insert(trên modal)
function onBtnCreateProjectClick() {
    //khai báo obj chứa data
    var vRequestDataObj = {
        name: "",
        provinceId: "",
        districtId: "",
        lat: "",
        lng: "",
        address: "",
        slogan: "",
        description: "",
        acreage: "",
        constructArea: "",
        numBlock: "",
        numFloors: "",
        numApartment: "",
        apartmentArea: "",
        investor: "",
        constructionContractor: "",
        designUnit: "",
        utilities: "",
        regionLink: "",
        photo: ""
    }
    //b1: get data
    getInsertProjectData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateProjectData(vRequestDataObj);
    //ghi ra console value trả ra của hàm validate
    console.log(vIsValid);
    if (vIsValid) {
        //b3: insert data
        $.ajax({
            url: gBASE_URL,
            type: "POST",
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleInsertProjectSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

//hàm xử lý sự kiện nút Update(trên modal)
function onBtnUpdateProjectClick() {
    //khai báo obj chứa data
    var vRequestDataObj = {
        name: "",
        provinceId: "",
        districtId: "",
        lat: "",
        lng: "",
        address: "",
        slogan: "",
        description: "",
        acreage: "",
        constructArea: "",
        numBlock: "",
        numFloors: "",
        numApartment: "",
        apartmentArea: "",
        investor: "",
        constructionContractor: "",
        designUnit: "",
        utilities: "",
        regionLink: "",
        photo: ""
    }
    //b1: get data
    getUpdateProjectData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateProjectData(vRequestDataObj);
    if (vIsValid) {
        //b3: update data
        $.ajax({
            url: gBASE_URL + gProjectId,
            type: "PUT",
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleUpdateProjectSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

// hàm xử lý sự kiện delete trên modal
function onBtnDeleteProjectConfirmClick() {
    $.ajax({
        url: gBASE_URL + gProjectId,
        type: "DELETE",
        contentType: "application/json;charset-UTF-8",
        success: function (paramRes) {
            // B4: xử lý front-end
            handleDeleteProjectSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
/*** NUM_APARTMENT 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to DataTable
function loadDataToTable(paramProject) {
    gProjectTable.clear();
    gProjectTable.rows.add(paramProject);
    gProjectTable.draw();
}
//hàm gọi api để lấy all list
function getAllProjects() {
    $.ajax({
        url: gBASE_URL,
        type: "GET",
        success: function (paramProject) {
            //ghi response ra console
            console.log(paramProject);
            loadDataToTable(paramProject);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
//hàm dựa vào button detail (edit or delete) xác định dc id obj
function getProjectIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    var vProjectRowData = gProjectTable.row(vTableRow).data();
    return vProjectRowData.id;
}

//hàm thu thập insert data
function getInsertProjectData(paramProjectObj) {
    paramProjectObj.name = $("#inp-create-project-name").val().trim();
    paramProjectObj.provinceId = $("#inp-create-project-provinceId").val().trim();
    paramProjectObj.districtId = $("#inp-create-project-districtId").val().trim();
    paramProjectObj.lat = $("#inp-create-project-lat").val().trim();
    paramProjectObj.lng = $("#inp-create-project-lng").val().trim();
    paramProjectObj.address = $("#inp-create-project-address").val().trim();
    paramProjectObj.slogan = $("#inp-create-project-slogan").val().trim();
    paramProjectObj.description = $("#inp-create-project-description").val().trim();
    paramProjectObj.acreage = $("#inp-create-project-acreage").val().trim();
    paramProjectObj.constructArea = $("#inp-create-project-constructArea").val().trim();
    paramProjectObj.numBlock = $("#inp-create-project-numBlock").val().trim();
    paramProjectObj.numFloors = $("#inp-create-project-numFloors").val().trim();
    paramProjectObj.numApartment = $("#inp-create-project-numApartment").val().trim();
    paramProjectObj.apartmentArea = $("#inp-create-project-apartmentArea").val().trim();
    paramProjectObj.investor = $("#inp-create-project-investor").val().trim();
    paramProjectObj.constructionContractor = $("#inp-create-project-constructionContractor").val().trim();
    paramProjectObj.designUnit = $("#inp-create-project-designUnit").val().trim();
    paramProjectObj.utilities = $("#inp-create-project-utilities").val().trim();
    paramProjectObj.regionLink = $("#inp-create-project-regionLink").val();
    var vPhotoInput = $("#inp-create-project-photo");
    var vFileName = vPhotoInput.val().split('\\').pop().split('/').pop();
    paramProjectObj.photo = vFileName;
}
//hàm validate data
function validateProjectData(paramProjectObj) {
    if (paramProjectObj.numApartment === "") {
        alert("Chưa nhập số căn hộ!");
        return false;
    }
    if (isNaN(paramProjectObj.numApartment)) {
        alert("Số căn hộ phải là số!");
        return false;
    }
    if (paramProjectObj.investor === "") {
        alert("Chưa nhập nhà đầu tư!");
        return false;
    }
    if (paramProjectObj.utilities === "") {
        alert("Chưa nhập tiện ích!");
        return false;
    }
    if (paramProjectObj.regionLink === "") {
        alert("Chưa nhập kết nối vùng!");
        return false;
    }

    return true;
}
//hàm xử lý front-end khi thêm obj thành công
function handleInsertProjectSuccess() {
    alert("Thêm mới thành công!");
    getAllProjects();
    resetCreateProjectForm();
    $("#create-project-modal").modal("hide");
}
//hàm xóa trắng form create
function resetCreateProjectForm() {
    $("#inp-create-project-name").val("");
    $("#inp-create-project-provinceId").val("");
    $("#inp-create-project-districtId").val("");
    $("#inp-create-project-lat").val("");
    $("#inp-create-project-lng").val("");
    $("#inp-create-project-address").val("");
    $("#inp-create-project-slogan").val("");
    $("#inp-create-project-description").val("");
    $("#inp-create-project-acreage").val("");
    $("#inp-create-project-constructArea").val("");
    $("#inp-create-project-numBlock").val("");
    $("#inp-create-project-numFloors").val("");
    $("#inp-create-project-numApartment").val("");
    $("#inp-create-project-apartmentArea").val("");
    $("#inp-create-project-investor").val("");
    $("#inp-create-project-constructionContractor").val("");
    $("#inp-create-project-designUnit").val("");
    $("#inp-create-project-utilities").val("");
    $("#inp-create-project-regionLink").val("");
    $("#inp-create-project-photo").val("");
}

//hàm load data lên update modal form
function getProjectDataByProjectId(paramProjectId) {
    $.ajax({
        url: gBASE_URL + paramProjectId,
        type: "GET",
        success: function (paramProjectId) {
            showProjectDataToUpdateModal(paramProjectId);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
    // hiển thị modal lên
    $("#update-project-modal").modal("show");
}
//hàm show data lên update modal form
function showProjectDataToUpdateModal(paramProjectDataObj) {
    $("#inp-update-project-id").val(paramProjectDataObj.id);
    $("#inp-update-project-name").val(paramProjectDataObj.name);
    $("#inp-update-project-provinceId").val(paramProjectDataObj.provinceId);
    $("#inp-update-project-districtId").val(paramProjectDataObj.districtId);
    $("#inp-update-project-lat").val(paramProjectDataObj.lat);
    $("#inp-update-project-lng").val(paramProjectDataObj.lng);
    $("#inp-update-project-address").val(paramProjectDataObj.address);
    $("#inp-update-project-slogan").val(paramProjectDataObj.slogan);
    $("#inp-update-project-description").val(paramProjectDataObj.description);
    $("#inp-update-project-acreage").val(paramProjectDataObj.acreage);
    $("#inp-update-project-constructArea").val(paramProjectDataObj.constructArea);
    $("#inp-update-project-numBlock").val(paramProjectDataObj.numBlock);
    $("#inp-update-project-numFloors").val(paramProjectDataObj.numFloors);
    $("#inp-update-project-numApartment").val(paramProjectDataObj.numApartment);
    $("#inp-update-project-apartmentArea").val(paramProjectDataObj.apartmentArea);
    $("#inp-update-project-investor").val(paramProjectDataObj.investor);
    $("#inp-update-project-constructionContractor").val(paramProjectDataObj.constructionContractor);
    $("#inp-update-project-designUnit").val(paramProjectDataObj.designUnit);
    $("#inp-update-project-utilities").val(paramProjectDataObj.utilities);
    $("#inp-update-project-regionLink").val(paramProjectDataObj.regionLink);
    // Hiển thị tên file trong một thẻ <span>
    var vFileName = paramProjectDataObj.photo.split('/').pop();
    $("#file-label").text(vFileName);
}
//hàm thu thập update data
function getUpdateProjectData(paramProjectDataObj) {
    paramProjectDataObj.name = $("#inp-update-project-name").val().trim();
    paramProjectDataObj.provinceId = $("#inp-update-project-provinceId").val().trim();
    paramProjectDataObj.districtId = $("#inp-update-project-districtId").val().trim();
    paramProjectDataObj.lat = $("#inp-update-project-lat").val().trim();
    paramProjectDataObj.lng = $("#inp-update-project-lng").val().trim();
    paramProjectDataObj.address = $("#inp-update-project-address").val().trim();
    paramProjectDataObj.slogan = $("#inp-update-project-slogan").val().trim();
    paramProjectDataObj.description = $("#inp-update-project-description").val().trim();
    paramProjectDataObj.acreage = $("#inp-update-project-acreage").val().trim();
    paramProjectDataObj.constructArea = $("#inp-update-project-constructArea").val().trim();
    paramProjectDataObj.numBlock = $("#inp-update-project-numBlock").val().trim();
    paramProjectDataObj.numFloors = $("#inp-update-project-numFloors").val().trim();
    paramProjectDataObj.numApartment = $("#inp-update-project-numApartment").val().trim();
    paramProjectDataObj.apartmentArea = $("#inp-update-project-apartmentArea").val().trim();
    paramProjectDataObj.investor = $("#inp-update-project-investor").val().trim();
    paramProjectDataObj.constructionContractor = $("#inp-update-project-constructionContractor").val().trim();
    paramProjectDataObj.designUnit = $("#inp-update-project-designUnit").val().trim();
    paramProjectDataObj.utilities = $("#inp-update-project-utilities").val().trim();
    paramProjectDataObj.regionLink = $("#inp-update-project-regionLink").val().trim();
    var vPhotoInput = $("#inp-update-project-photo");
    var vFileName = vPhotoInput.val().split('\\').pop().split('/').pop();
    paramProjectDataObj.photo = vFileName;
}
//hàm xử lý front-end khi update data thành công
function handleUpdateProjectSuccess() {
    alert("Cập nhật thành công!");
    getAllProjects();
    $("#update-project-modal").modal("hide");
}
//hàm xử lý front-end khi delete thành công
function handleDeleteProjectSuccess() {
    alert("Xóa thành công!");
    getAllProjects();
    $("#delete-project-modal").modal("hide");
}