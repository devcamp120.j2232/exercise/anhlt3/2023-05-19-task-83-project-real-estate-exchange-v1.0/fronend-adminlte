"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/construction-contractors/";
// biến toàn cục để lưu trữ id đang dc update or delete. Mặc định = 0
var gContractorId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gCONTRACTOR_COLS = ["id", "name", "address", "phone", "phone2", "fax", "email", "action"];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gID_COL = 0;
const gNAME_COL = 1;
const gADDRESS_COL = 2;
const gPHONE_COL = 3;
const gPHONE2_COL = 4;
const gFAX_COL = 5;
const gEMAIL_COL = 6;
const gACTION_COL = 7;

//khai báo DataTable & mapping columns
var gContractorTable = $("#contractor-table").DataTable({
    columns: [
        { data: gCONTRACTOR_COLS[gID_COL] },
        { data: gCONTRACTOR_COLS[gNAME_COL] },
        { data: gCONTRACTOR_COLS[gADDRESS_COL] },
        { data: gCONTRACTOR_COLS[gPHONE_COL] },
        { data: gCONTRACTOR_COLS[gPHONE2_COL] },
        { data: gCONTRACTOR_COLS[gFAX_COL] },
        { data: gCONTRACTOR_COLS[gEMAIL_COL] },
        { data: gCONTRACTOR_COLS[gACTION_COL] }
    ],
    columnDefs: [
        {
            targets: gACTION_COL,
            defaultContent: `
            <i class="fas fa-edit edit-contractor" style="cursor:pointer;color:blue;"></i>
            <i class="fas fa-trash delete-contractor" style="cursor:pointer;color:red;"></i>
          `
        }
    ]
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
});
//2 - C: gán sự kiện Create-thêm mới
$("#btn-add-new-contractor").on("click", function () {
    onBtnAddNewContractorClick();
});
//3 - U: gán sự kiện update-sửa
$("#contractor-table").on("click", ".edit-contractor", function () {
    onBtnEditContractorClick(this);
});
//4 - D: gán sự kiện delete-xóa
$("#contractor-table").on("click", ".delete-contractor", function () {
    onBtnDeleteContractorClick(this);
});

//gán sự kiện cho nút Create(trên modal)
$("#btn-create-contractor").on("click", function () {
    onBtnCreateContractorClick();
});
//gán sự kiện cho nút Update(trên modal)
$("#btn-update-contractor").on("click", function () {
    onBtnUpdateContractorClick();
});
//gán sự kiện cho nút Delete(trên modal)
$("#btn-confirm-delete-contractor").on("click", function () {
    onBtnDeleteContractorConfirmClick();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    //1 - R: read/load to table
    getAllContractors();
}
//hàm xử lý sự kiện nút Thêm mới
function onBtnAddNewContractorClick() {
    //hiển thị modal trắng lên
    $("#create-contractor-modal").modal("show");
}
//hàm xử lý sự kiện Update từ table
function onBtnEditContractorClick(paramEdit) {
    //lưu thông tin Id đang dc edit vào biến toàn cục
    gContractorId = getContractorIdFromButton(paramEdit);
    console.log("Id của obj tương ứng = " + gContractorId);
    // load dữ liệu vào các trường dữ liệu trong modal
    getContractorDataByContractorId(gContractorId);
}
//hàm xử lý sự kiện Delete từ table
function onBtnDeleteContractorClick(paramDelete) {
    //lưu thông tin Id đang dc xóa vào biến toàn cục
    gContractorId = getContractorIdFromButton(paramDelete);
    console.log("Id của obj tương ứng = " + gContractorId);
    $("#delete-contractor-modal").modal("show");
}

//hàm xử lý sự kiện nút Insert(trên modal)
function onBtnCreateContractorClick() {
    //khai báo obj chứa data
    var vRequestDataObj = {
        name: "",
        description: "",
        projects: "",
        address: "",
        phone: "",
        phone2: "",
        fax: "",
        email: "",
        website: "",
        note: ""
    }
    //b1: get data
    getInsertContractorData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateContractorData(vRequestDataObj);
    //ghi ra console value trả ra của hàm validate
    console.log(vIsValid);
    if (vIsValid) {
        //b3: insert data
        $.ajax({
            url: gBASE_URL,
            type: "POST",
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleInsertContractorSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

//hàm xử lý sự kiện nút Update(trên modal)
function onBtnUpdateContractorClick() {
    //khai báo obj chứa data
    var vRequestDataObj = {
        name: "",
        description: "",
        projects: "",
        address: "",
        phone: "",
        phone2: "",
        fax: "",
        email: "",
        website: "",
        note: ""
    }
    //b1: get data
    getUpdateContractorData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateContractorData(vRequestDataObj);
    if (vIsValid) {
        //b3: update data
        $.ajax({
            url: gBASE_URL + gContractorId,
            type: "PUT",
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleUpdateContractorSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

// hàm xử lý sự kiện delete trên modal
function onBtnDeleteContractorConfirmClick() {
    $.ajax({
        url: gBASE_URL + gContractorId,
        type: "DELETE",
        contentType: "application/json;charset-UTF-8",
        success: function (paramRes) {
            // B4: xử lý front-end
            handleDeleteContractorSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to DataTable
function loadDataToTable(paramContractor) {
    gContractorTable.clear();
    gContractorTable.rows.add(paramContractor);
    gContractorTable.draw();
}
//hàm gọi api để lấy all list
function getAllContractors() {
    $.ajax({
        url: gBASE_URL,
        type: "GET",
        success: function (paramContractor) {
            //ghi response ra console
            console.log(paramContractor);
            loadDataToTable(paramContractor);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
//hàm dựa vào button detail (edit or delete) xác định dc id obj
function getContractorIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    var vContractorRowData = gContractorTable.row(vTableRow).data();
    return vContractorRowData.id;
}

//hàm thu thập insert data
function getInsertContractorData(paramContractorObj) {
    paramContractorObj.name = $("#inp-create-contractor-name").val().trim();
    paramContractorObj.description = $("#inp-create-contractor-description").val().trim();
    paramContractorObj.projects = $("#inp-create-contractor-projects").val().trim();
    paramContractorObj.address = $("#inp-create-contractor-address").val().trim();
    paramContractorObj.phone = $("#inp-create-contractor-phone").val().trim();
    paramContractorObj.phone2 = $("#inp-create-contractor-phone2").val().trim();
    paramContractorObj.fax = $("#inp-create-contractor-fax").val().trim();
    paramContractorObj.email = $("#inp-create-contractor-email").val().trim();
    paramContractorObj.website = $("#inp-create-contractor-website").val().trim();
    paramContractorObj.note = $("#inp-create-contractor-note").val().trim();
}
//hàm validate data
function validateContractorData(paramContractorObj) {
    if (paramContractorObj.name === "") {
        alert("Chưa nhập tên nhà thầu!");
        return false;
    }

    return true;
}
//hàm xử lý front-end khi thêm obj thành công
function handleInsertContractorSuccess() {
    alert("Thêm mới thành công!");
    getAllContractors();
    resetCreateContractorForm();
    $("#create-contractor-modal").modal("hide");
}
//hàm xóa trắng form create
function resetCreateContractorForm() {
    $("#inp-create-contractor-name").val("");
    $("#inp-create-contractor-description").val("");
    $("#inp-create-contractor-projects").val("");
    $("#inp-create-contractor-address").val("");
    $("#inp-create-contractor-phone").val("");
    $("#inp-create-contractor-phone2").val("");
    $("#inp-create-contractor-fax").val("");
    $("#inp-create-contractor-email").val("");
    $("#inp-create-contractor-website").val("");
    $("#inp-create-contractor-note").val("");
}

//hàm load data lên update modal form
function getContractorDataByContractorId(paramContractorId) {
    $.ajax({
        url: gBASE_URL + paramContractorId,
        type: "GET",
        success: function (paramContractorId) {
            showContractorDataToUpdateModal(paramContractorId);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
    // hiển thị modal lên
    $("#update-contractor-modal").modal("show");
}
//hàm show data lên update modal form
function showContractorDataToUpdateModal(paramContractorDataObj) {
    $("#inp-update-contractor-id").val(paramContractorDataObj.id);
    $("#inp-update-contractor-name").val(paramContractorDataObj.name);
    $("#inp-update-contractor-description").val(paramContractorDataObj.description);
    $("#inp-update-contractor-projects").val(paramContractorDataObj.projects);
    $("#inp-update-contractor-address").val(paramContractorDataObj.address);
    $("#inp-update-contractor-phone").val(paramContractorDataObj.phone);
    $("#inp-update-contractor-phone2").val(paramContractorDataObj.phone2);
    $("#inp-update-contractor-fax").val(paramContractorDataObj.fax);
    $("#inp-update-contractor-email").val(paramContractorDataObj.email);
    $("#inp-update-contractor-website").val(paramContractorDataObj.website);
    $("#inp-update-contractor-note").val(paramContractorDataObj.note);
}
//hàm thu thập update data
function getUpdateContractorData(paramContractorDataObj) {
    paramContractorDataObj.name = $("#inp-update-contractor-name").val().trim();
    paramContractorDataObj.description = $("#inp-update-contractor-description").val().trim();
    paramContractorDataObj.projects = $("#inp-update-contractor-projects").val().trim();
    paramContractorDataObj.address = $("#inp-update-contractor-address").val().trim();
    paramContractorDataObj.phone = $("#inp-update-contractor-phone").val().trim();
    paramContractorDataObj.phone2 = $("#inp-update-contractor-phone2").val().trim();
    paramContractorDataObj.fax = $("#inp-update-contractor-fax").val().trim();
    paramContractorDataObj.email = $("#inp-update-contractor-email").val().trim();
    paramContractorDataObj.website = $("#inp-update-contractor-website").val().trim();
    paramContractorDataObj.note = $("#inp-update-contractor-note").val().trim();
}
//hàm xử lý front-end khi update data thành công
function handleUpdateContractorSuccess() {
    alert("Cập nhật thành công!");
    getAllContractors();
    $("#update-contractor-modal").modal("hide");
}
//hàm xử lý front-end khi delete thành công
function handleDeleteContractorSuccess() {
    alert("Xóa thành công!");
    getAllContractors();
    $("#delete-contractor-modal").modal("hide");
}