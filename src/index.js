"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/address-maps/";
// biến toàn cục để lưu trữ id class đang dc update or delete. Mặc định = 0
var gAMId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gADDRESS_MAP_COLS = ["id", "address", "lat", "lng"];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gAM_ID_COL = 0;
const gAM_ADDRESS_COL = 1;
const gAM_LAT_COL = 2;
const gAM_LNG_COL = 3;

//khai báo DataTable & mapping columns
var gAMTable = $("#adressmap-table").DataTable({
  columns: [
    { data: gADDRESS_MAP_COLS[gAM_ID_COL] },
    { data: gADDRESS_MAP_COLS[gAM_ADDRESS_COL] },
    { data: gADDRESS_MAP_COLS[gAM_LAT_COL] },
    { data: gADDRESS_MAP_COLS[gAM_LNG_COL] },
  ]
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  onPageLoading();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  //1 - R: read/load am to table
  getAllAddressMaps();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to DataTable
function loadDataToTable(paramAM) {
  gAMTable.clear();
  gAMTable.rows.add(paramAM);
  gAMTable.draw();
}
//hàm gọi api để lấy all list am
function getAllAddressMaps() {
  $.ajax({
    url: gBASE_URL,
    headers: { 'Authorization': 'Bearer ' + getCookie("token") },
    type: "GET",
    success: function (paramAM) {
      //ghi response ra console
      console.log(paramAM);
      loadDataToTable(paramAM);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}

//Hàm get Cookie 
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
