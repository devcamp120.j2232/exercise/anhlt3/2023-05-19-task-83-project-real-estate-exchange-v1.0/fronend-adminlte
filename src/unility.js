"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/unilities/";
// biến toàn cục để lưu trữ id đang dc update or delete. Mặc định = 0
var gUnilityId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gUNILITY_COLS = ["id", "name", "action"];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gID_COL = 0;
const gNAME_COL = 1;
const gACTION_COL = 2;

//khai báo DataTable & mapping columns
var gUnilityTable = $("#unility-table").DataTable({
    columns: [
        { data: gUNILITY_COLS[gID_COL] },
        { data: gUNILITY_COLS[gNAME_COL] },
        { data: gUNILITY_COLS[gACTION_COL] }
    ],
    columnDefs: [
        {
            targets: gACTION_COL,
            defaultContent: `
            <i class="fas fa-edit edit-unility" style="cursor:pointer;color:blue;"></i>
            <i class="fas fa-trash delete-unility" style="cursor:pointer;color:red;"></i>
          `
        }
    ]
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
});
//2 - C: gán sự kiện Create-thêm mới
$("#btn-add-new-unility").on("click", function () {
    onBtnAddNewUnilityClick();
});
//3 - U: gán sự kiện update-sửa
$("#unility-table").on("click", ".edit-unility", function () {
    onBtnEditUnilityClick(this);
});
//4 - D: gán sự kiện delete-xóa
$("#unility-table").on("click", ".delete-unility", function () {
    onBtnDeleteUnilityClick(this);
});

//gán sự kiện cho nút Create(trên modal)
$("#btn-create-unility").on("click", function () {
    onBtnCreateUnilityClick();
});
//gán sự kiện cho nút Update(trên modal)
$("#btn-update-unility").on("click", function () {
    onBtnUpdateUnilityClick();
});
//gán sự kiện cho nút Delete(trên modal)
$("#btn-confirm-delete-unility").on("click", function () {
    onBtnDeleteUnilityConfirmClick();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    //1 - R: read/load to table
    getAllUnilities();
}
//hàm xử lý sự kiện nút Thêm mới
function onBtnAddNewUnilityClick() {
    //hiển thị modal trắng lên
    $("#create-unility-modal").modal("show");
}
//hàm xử lý sự kiện Update từ table
function onBtnEditUnilityClick(paramEdit) {
    //lưu thông tin Id đang dc edit vào biến toàn cục
    gUnilityId = getUnilityIdFromButton(paramEdit);
    console.log("Id của obj tương ứng = " + gUnilityId);
    // load dữ liệu vào các trường dữ liệu trong modal
    getUnilityDataByUnilityId(gUnilityId);
}
//hàm xử lý sự kiện Delete từ table
function onBtnDeleteUnilityClick(paramDelete) {
    //lưu thông tin Id đang dc xóa vào biến toàn cục
    gUnilityId = getUnilityIdFromButton(paramDelete);
    console.log("Id của obj tương ứng = " + gUnilityId);
    $("#delete-unility-modal").modal("show");
}

//hàm xử lý sự kiện nút Insert(trên modal)
function onBtnCreateUnilityClick() {
    //khai báo obj chứa data
    var vRequestDataObj = {
        name: "",
        description: "",
        photo: ""
    }
    //b1: get data
    getInsertUnilityData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateUnilityData(vRequestDataObj);
    //ghi ra console value trả ra của hàm validate
    console.log(vIsValid);
    if (vIsValid) {
        //b3: insert data
        $.ajax({
            url: gBASE_URL,
            type: "POST",
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleInsertUnilitySuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

//hàm xử lý sự kiện nút Update(trên modal)
function onBtnUpdateUnilityClick() {
    //khai báo obj chứa data
    var vRequestDataObj = {
        name: "",
        description: "",
        photo: ""
    }
    //b1: get data
    getUpdateUnilityData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateUnilityData(vRequestDataObj);
    if (vIsValid) {
        //b3: update data
        $.ajax({
            url: gBASE_URL + gUnilityId,
            type: "PUT",
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleUpdateUnilitySuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

// hàm xử lý sự kiện delete trên modal
function onBtnDeleteUnilityConfirmClick() {
    $.ajax({
        url: gBASE_URL + gUnilityId,
        type: "DELETE",
        contentType: "application/json;charset-UTF-8",
        success: function (paramRes) {
            // B4: xử lý front-end
            handleDeleteUnilitySuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to DataTable
function loadDataToTable(paramUnility) {
    gUnilityTable.clear();
    gUnilityTable.rows.add(paramUnility);
    gUnilityTable.draw();
}
//hàm gọi api để lấy all list
function getAllUnilities() {
    $.ajax({
        url: gBASE_URL,
        type: "GET",
        success: function (paramUnility) {
            //ghi response ra console
            console.log(paramUnility);
            loadDataToTable(paramUnility);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
//hàm dựa vào button detail (edit or delete) xác định dc id obj
function getUnilityIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    var vUnilityRowData = gUnilityTable.row(vTableRow).data();
    return vUnilityRowData.id;
}

//hàm thu thập insert data
function getInsertUnilityData(paramUnilityObj) {
    paramUnilityObj.name = $("#inp-create-unility-name").val().trim();
    paramUnilityObj.description = $("#inp-create-unility-description").val().trim();
    var vPhotoInput = $("#inp-create-unility-photo");
    var vFileName = vPhotoInput.val().split('\\').pop().split('/').pop();
    paramUnilityObj.photo = vFileName;
}
//hàm validate data
function validateUnilityData(paramUnilityObj) {
    if (paramUnilityObj.name === "") {
        alert("Chưa nhập tên!");
        return false;
    }

    return true;
}
//hàm xử lý front-end khi thêm obj thành công
function handleInsertUnilitySuccess() {
    alert("Thêm mới thành công!");
    getAllUnilities();
    resetCreateUnilityForm();
    $("#create-unility-modal").modal("hide");
}
//hàm xóa trắng form create
function resetCreateUnilityForm() {
    $("#inp-create-unility-name").val("");
    $("#inp-create-unility-description").val("");
    $("#inp-create-unility-photo").val("");
}

//hàm load data lên update modal form
function getUnilityDataByUnilityId(paramUnilityId) {
    $.ajax({
        url: gBASE_URL + paramUnilityId,
        type: "GET",
        success: function (paramUnilityId) {
            showUnilityDataToUpdateModal(paramUnilityId);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
    // hiển thị modal lên
    $("#update-unility-modal").modal("show");
}
//hàm show data lên update modal form
function showUnilityDataToUpdateModal(paramUnilityDataObj) {
    $("#inp-update-unility-id").val(paramUnilityDataObj.id);
    $("#inp-update-unility-name").val(paramUnilityDataObj.name);
    $("#inp-update-unility-description").val(paramUnilityDataObj.description);
    // Hiển thị tên file trong một thẻ <span>
    var vFileName = paramUnilityDataObj.photo.split('/').pop();
    $("#file-label").text(vFileName);
}
//hàm thu thập update data
function getUpdateUnilityData(paramUnilityDataObj) {
    paramUnilityDataObj.name = $("#inp-update-unility-name").val().trim();
    paramUnilityDataObj.description = $("#inp-update-unility-description").val().trim();
    var vPhotoInput = $("#inp-update-unility-photo");
    var vFileName = vPhotoInput.val().split('\\').pop().split('/').pop();
    paramUnilityDataObj.photo = vFileName;  
}
//hàm xử lý front-end khi update data thành công
function handleUpdateUnilitySuccess() {
    alert("Cập nhật thành công!");
    getAllUnilities();
    $("#update-unility-modal").modal("hide");
}
//hàm xử lý front-end khi delete thành công
function handleDeleteUnilitySuccess() {
    alert("Xóa thành công!");
    getAllUnilities();
    $("#delete-unility-modal").modal("hide");
}