"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/address-maps/";
// biến toàn cục để lưu trữ id class đang dc update or delete. Mặc định = 0
var gAMId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gADDRESS_MAP_COLS = ["id", "address", "lat", "lng", "action"];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gAM_ID_COL = 0;
const gAM_ADDRESS_COL = 1;
const gAM_LAT_COL = 2;
const gAM_LNG_COL = 3;
const gAM_ACTION_COL = 4;

//khai báo DataTable & mapping columns
var gAMTable = $("#adressmap-table").DataTable({
  columns: [
    { data: gADDRESS_MAP_COLS[gAM_ID_COL] },
    { data: gADDRESS_MAP_COLS[gAM_ADDRESS_COL] },
    { data: gADDRESS_MAP_COLS[gAM_LAT_COL] },
    { data: gADDRESS_MAP_COLS[gAM_LNG_COL] },
    { data: gADDRESS_MAP_COLS[gAM_ACTION_COL] }
  ],
  columnDefs: [
    {
      targets: gAM_ACTION_COL,
      defaultContent: `
            <i class="fas fa-edit edit-am" style="cursor:pointer;color:blue;"></i>
            <i class="fas fa-trash delete-am" style="cursor:pointer;color:red;"></i>
          `
    }
  ]
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  onPageLoading();
});
//2 - C: gán sự kiện Create-thêm mới am
$("#btn-add-new-adressmap").on("click", function () {
  onBtnAddNewAMClick();
});
//3 - U: gán sự kiện update-sửa 1 am
$("#adressmap-table").on("click", ".edit-am", function () {
  onBtnEditAMClick(this);
});
//4 - D: gán sự kiện delete-xóa 1 am
$("#adressmap-table").on("click", ".delete-am", function () {
  onBtnDeleteAMClick(this);
});

//gán sự kiện cho nút Create am(trên modal)
$("#btn-create-addressmap").on("click", function () {
  onBtnCreateAMClick();
});
//gán sự kiện cho nút Update am(trên modal)
$("#btn-update-addressmap").on("click", function () {
  onBtnUpdateAMClick();
});
//gán sự kiện cho nút Delete am(trên modal)
$("#btn-confirm-delete-addressmap").on("click", function () {
  onBtnDeleteAMConfirmClick();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  //1 - R: read/load am to table
  getAllAddressMaps();
}
//hàm xử lý sự kiện nút Thêm mới
function onBtnAddNewAMClick() {
  //hiển thị modal trắng lên
  $("#create-addressmap-modal").modal("show");
}
//hàm xử lý sự kiện Update am từ table
function onBtnEditAMClick(paramEdit) {
  //lưu thông tin Id đang dc edit vào biến toàn cục
  gAMId = getAMIdFromButton(paramEdit);
  console.log("Id của obj tương ứng = " + gAMId);
  // load dữ liệu vào các trường dữ liệu trong modal
  getAMDataByAMId(gAMId);
}
//hàm xử lý sự kiện Delete am từ table
function onBtnDeleteAMClick(paramDelete) {
  //lưu thông tin Id đang dc xóa vào biến toàn cục
  gAMId = getAMIdFromButton(paramDelete);
  console.log("Id của obj tương ứng = " + gAMId);
  $("#delete-addressmap-modal").modal("show");
}

//hàm xử lý sự kiện nút Insert(trên modal)
function onBtnCreateAMClick() {
  //khai báo obj chứa am data
  var vRequestDataObj = {
    address: "",
    lat: "",
    lng: ""
  }
  //b1: get data
  getInsertAMData(vRequestDataObj);
  //b2: validate data
  var vIsValid = validateAMData(vRequestDataObj);
  //ghi ra console value trả ra của hàm validateAMData
  console.log(vIsValid);
  if (vIsValid) {
    //b3: insert am
    $.ajax({
      url: gBASE_URL,
      type: "POST",
      contentType: "application/json;charset-UTF-8",
      data: JSON.stringify(vRequestDataObj),
      success: function (paramRes) {
        console.log(paramRes);
        //b4: xử lý front-end
        handleInsertAMSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

//hàm xử lý sự kiện nút Update(trên modal)
function onBtnUpdateAMClick() {
  //khai báo obj chứa am data
  var vRequestDataObj = {
    address: "",
    lat: "",
    lng: ""
  }
  //b1: get data
  getUpdateAMData(vRequestDataObj);
  //b2: validate data
  var vIsValid = validateAMData(vRequestDataObj);
  if (vIsValid) {
    //b3: update am
    $.ajax({
      url: gBASE_URL + gAMId,
      type: "PUT",
      contentType: "application/json;charset-UTF-8",
      data: JSON.stringify(vRequestDataObj),
      success: function (paramRes) {
        console.log(paramRes);
        //b4: xử lý front-end
        handleUpdateAMSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

// hàm xử lý sự kiện delete trên modal
function onBtnDeleteAMConfirmClick() {
  $.ajax({
    url: gBASE_URL + gAMId,
    type: "DELETE",
    contentType: "application/json;charset-UTF-8",
    success: function (paramRes) {
      // B4: xử lý front-end
      handleDeleteAMSuccess();
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to DataTable
function loadDataToTable(paramAM) {
  gAMTable.clear();
  gAMTable.rows.add(paramAM);
  gAMTable.draw();
}
//hàm gọi api để lấy all list am
function getAllAddressMaps() {
  console.log('Bearer ' + getCookie("token"));
  $.ajax({
    url: gBASE_URL,
    headers: { 'Authorization': 'Bearer ' + getCookie("token") },
    type: "GET",
    success: function (paramAM) {
      //ghi response ra console
      console.log(paramAM);
      console.log('Bearer ' + getCookie("token"));
      loadDataToTable(paramAM);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}

//Hàm get Cookie 
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
          c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
      }
  }
  return "";
}
//hàm dựa vào button detail (edit or delete) xác định dc id obj
function getAMIdFromButton(paramButton) {
  var vTableRow = $(paramButton).parents("tr");
  var vAMRowData = gAMTable.row(vTableRow).data();
  return vAMRowData.id;
}

//hàm thu thập insert data
function getInsertAMData(paramAMObj) {
  paramAMObj.address = $("#inp-create-addressmap-address").val().trim();
  paramAMObj.lat = $("#inp-create-addressmap-lat").val().trim();
  paramAMObj.lng = $("#inp-create-addressmap-lng").val().trim();
}
//hàm validate data
function validateAMData(paramAMObj) {
  if (paramAMObj.address === "") {
    alert("Chưa nhập địa chỉ!");
    return false;
  }

  if (paramAMObj.lat === "") {
    alert("Chưa nhập vĩ độ lat!");
    return false;
  }

  if (isNaN(paramAMObj.lat)) {
    alert("Vĩ độ lat phải là số!");
    return false;
  }

  if (paramAMObj.lng === "") {
    alert("Chưa nhập kinh độ lng!");
    return false;
  }

  if (isNaN(paramAMObj.lng)) {
    alert("Kinh độ lng phải là số!");
    return false;
  }
  return true;
}
//hàm xử lý front-end khi thêm obj thành công
function handleInsertAMSuccess() {
  alert("Thêm mới thành công!");
  getAllAddressMaps();
  resetCreateAMForm();
  $("#create-addressmap-modal").modal("hide");
}
//hàm xóa trắng form create
function resetCreateAMForm() {
  $("#inp-create-addressmap-address").val("");
  $("#inp-create-addressmap-lat").val("");
  $("#inp-create-addressmap-lng").val("");
}

//hàm load am data lên update modal form
function getAMDataByAMId(paramAMId) {
  $.ajax({
    url: gBASE_URL + paramAMId,
    type: "GET",
    success: function (paramAMId) {
      showAMDataToUpdateModal(paramAMId);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
  // hiển thị modal lên
  $("#update-addressmap-modal").modal("show");
}
//hàm show am data lên update modal form
function showAMDataToUpdateModal(paramAMDataObj) {
  $("#inp-update-addressmap-id").val(paramAMDataObj.id);
  $("#inp-update-addressmap-address").val(paramAMDataObj.address);
  $("#inp-update-addressmap-lat").val(paramAMDataObj.lat);
  $("#inp-update-addressmap-lng").val(paramAMDataObj.lng);
}
//hàm thu thập update data
function getUpdateAMData(paramAMDataObj) {
  paramAMDataObj.address = $("#inp-update-addressmap-address").val().trim();
  paramAMDataObj.lat = $("#inp-update-addressmap-lat").val().trim();
  paramAMDataObj.lng = $("#inp-update-addressmap-lng").val().trim();
}
//hàm xử lý front-end khi update data thành công
function handleUpdateAMSuccess() {
  alert("Cập nhật thành công!");
  getAllAddressMaps();
  $("#update-addressmap-modal").modal("hide");
}
//hàm xử lý front-end khi delete thành công
function handleDeleteAMSuccess() {
  alert("Xóa thành công!");
  getAllAddressMaps();
  $("#delete-addressmap-modal").modal("hide");
}