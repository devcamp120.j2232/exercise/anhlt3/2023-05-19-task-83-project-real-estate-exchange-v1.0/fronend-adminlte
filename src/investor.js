"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/investors/";
// biến toàn cục để lưu trữ id đang dc update or delete. Mặc định = 0
var gInvestorId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gINVESTOR_COLS = ["id", "name", "address", "phone", "phone2", "fax", "email", "action"];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gID_COL = 0;
const gNAME_COL = 1;
const gADDRESS_COL = 2;
const gPHONE_COL = 3;
const gPHONE2_COL = 4;
const gFAX_COL = 5;
const gEMAIL_COL = 6;
const gACTION_COL = 7;

//khai báo DataTable & mapping columns
var gInvestorTable = $("#investor-table").DataTable({
    columns: [
        { data: gINVESTOR_COLS[gID_COL] },
        { data: gINVESTOR_COLS[gNAME_COL] },
        { data: gINVESTOR_COLS[gADDRESS_COL] },
        { data: gINVESTOR_COLS[gPHONE_COL] },
        { data: gINVESTOR_COLS[gPHONE2_COL] },
        { data: gINVESTOR_COLS[gFAX_COL] },
        { data: gINVESTOR_COLS[gEMAIL_COL] },
        { data: gINVESTOR_COLS[gACTION_COL] }
    ],
    columnDefs: [
        {
            targets: gACTION_COL,
            defaultContent: `
            <i class="fas fa-edit edit-investor" style="cursor:pointer;color:blue;"></i>
            <i class="fas fa-trash delete-investor" style="cursor:pointer;color:red;"></i>
          `
        }
    ]
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
});
//2 - C: gán sự kiện Create-thêm mới
$("#btn-add-new-investor").on("click", function () {
    onBtnAddNewInvestorClick();
});
//3 - U: gán sự kiện update-sửa
$("#investor-table").on("click", ".edit-investor", function () {
    onBtnEditInvestorClick(this);
});
//4 - D: gán sự kiện delete-xóa
$("#investor-table").on("click", ".delete-investor", function () {
    onBtnDeleteInvestorClick(this);
});

//gán sự kiện cho nút Create(trên modal)
$("#btn-create-investor").on("click", function () {
    onBtnCreateInvestorClick();
});
//gán sự kiện cho nút Update(trên modal)
$("#btn-update-investor").on("click", function () {
    onBtnUpdateInvestorClick();
});
//gán sự kiện cho nút Delete(trên modal)
$("#btn-confirm-delete-investor").on("click", function () {
    onBtnDeleteInvestorConfirmClick();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    //1 - R: read/load to table
    getAllinvestors();
}
//hàm xử lý sự kiện nút Thêm mới
function onBtnAddNewInvestorClick() {
    //hiển thị modal trắng lên
    $("#create-investor-modal").modal("show");
}
//hàm xử lý sự kiện Update từ table
function onBtnEditInvestorClick(paramEdit) {
    //lưu thông tin Id đang dc edit vào biến toàn cục
    gInvestorId = getInvestorIdFromButton(paramEdit);
    console.log("Id của obj tương ứng = " + gInvestorId);
    // load dữ liệu vào các trường dữ liệu trong modal
    getInvestorDataByInvestorId(gInvestorId);
}
//hàm xử lý sự kiện Delete từ table
function onBtnDeleteInvestorClick(paramDelete) {
    //lưu thông tin Id đang dc xóa vào biến toàn cục
    gInvestorId = getInvestorIdFromButton(paramDelete);
    console.log("Id của obj tương ứng = " + gInvestorId);
    $("#delete-investor-modal").modal("show");
}

//hàm xử lý sự kiện nút Insert(trên modal)
function onBtnCreateInvestorClick() {
    //khai báo obj chứa data
    var vRequestDataObj = {
        name: "",
        description: "",
        projects: "",
        address: "",
        phone: "",
        phone2: "",
        fax: "",
        email: "",
        website: "",
        note: ""
    }
    //b1: get data
    getInsertInvestorData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateInvestorData(vRequestDataObj);
    //ghi ra console value trả ra của hàm validate
    console.log(vIsValid);
    if (vIsValid) {
        //b3: insert data
        $.ajax({
            url: gBASE_URL,
            type: "POST",
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleInsertInvestorSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

//hàm xử lý sự kiện nút Update(trên modal)
function onBtnUpdateInvestorClick() {
    //khai báo obj chứa data
    var vRequestDataObj = {
        name: "",
        description: "",
        projects: "",
        address: "",
        phone: "",
        phone2: "",
        fax: "",
        email: "",
        website: "",
        note: ""
    }
    //b1: get data
    getUpdateInvestorData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateInvestorData(vRequestDataObj);
    if (vIsValid) {
        //b3: update data
        $.ajax({
            url: gBASE_URL + gInvestorId,
            type: "PUT",
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleUpdateInvestorSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

// hàm xử lý sự kiện delete trên modal
function onBtnDeleteInvestorConfirmClick() {
    $.ajax({
        url: gBASE_URL + gInvestorId,
        type: "DELETE",
        contentType: "application/json;charset-UTF-8",
        success: function (paramRes) {
            // B4: xử lý front-end
            handleDeleteInvestorSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to DataTable
function loadDataToTable(paramInvestor) {
    gInvestorTable.clear();
    gInvestorTable.rows.add(paramInvestor);
    gInvestorTable.draw();
}
//hàm gọi api để lấy all list
function getAllinvestors() {
    $.ajax({
        url: gBASE_URL,
        type: "GET",
        success: function (paramInvestor) {
            //ghi response ra console
            console.log(paramInvestor);
            loadDataToTable(paramInvestor);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
//hàm dựa vào button detail (edit or delete) xác định dc id obj
function getInvestorIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    var vInvestorRowData = gInvestorTable.row(vTableRow).data();
    return vInvestorRowData.id;
}

//hàm thu thập insert data
function getInsertInvestorData(paramInvestorObj) {
    paramInvestorObj.name = $("#inp-create-investor-name").val().trim();
    paramInvestorObj.description = $("#inp-create-investor-description").val().trim();
    paramInvestorObj.projects = $("#inp-create-investor-projects").val().trim();
    paramInvestorObj.address = $("#inp-create-investor-address").val().trim();
    paramInvestorObj.phone = $("#inp-create-investor-phone").val().trim();
    paramInvestorObj.phone2 = $("#inp-create-investor-phone2").val().trim();
    paramInvestorObj.fax = $("#inp-create-investor-fax").val().trim();
    paramInvestorObj.email = $("#inp-create-investor-email").val().trim();
    paramInvestorObj.website = $("#inp-create-investor-website").val().trim();
    paramInvestorObj.note = $("#inp-create-investor-note").val().trim();
}
//hàm validate data
function validateInvestorData(paramInvestorObj) {
    if (paramInvestorObj.name === "") {
        alert("Chưa nhập tên chủ đầu tư!");
        return false;
    }

    return true;
}
//hàm xử lý front-end khi thêm obj thành công
function handleInsertInvestorSuccess() {
    alert("Thêm mới thành công!");
    getAllinvestors();
    resetCreateInvestorForm();
    $("#create-investor-modal").modal("hide");
}
//hàm xóa trắng form create
function resetCreateInvestorForm() {
    $("#inp-create-investor-name").val("");
    $("#inp-create-investor-description").val("");
    $("#inp-create-investor-projects").val("");
    $("#inp-create-investor-address").val("");
    $("#inp-create-investor-phone").val("");
    $("#inp-create-investor-phone2").val("");
    $("#inp-create-investor-fax").val("");
    $("#inp-create-investor-email").val("");
    $("#inp-create-investor-website").val("");
    $("#inp-create-investor-note").val("");
}

//hàm load data lên update modal form
function getInvestorDataByInvestorId(paramInvestorId) {
    $.ajax({
        url: gBASE_URL + paramInvestorId,
        type: "GET",
        success: function (paramInvestorId) {
            showInvestorDataToUpdateModal(paramInvestorId);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
    // hiển thị modal lên
    $("#update-investor-modal").modal("show");
}
//hàm show data lên update modal form
function showInvestorDataToUpdateModal(paramInvestorDataObj) {
    $("#inp-update-investor-id").val(paramInvestorDataObj.id);
    $("#inp-update-investor-name").val(paramInvestorDataObj.name);
    $("#inp-update-investor-description").val(paramInvestorDataObj.description);
    $("#inp-update-investor-projects").val(paramInvestorDataObj.projects);
    $("#inp-update-investor-address").val(paramInvestorDataObj.address);
    $("#inp-update-investor-phone").val(paramInvestorDataObj.phone);
    $("#inp-update-investor-phone2").val(paramInvestorDataObj.phone2);
    $("#inp-update-investor-fax").val(paramInvestorDataObj.fax);
    $("#inp-update-investor-email").val(paramInvestorDataObj.email);
    $("#inp-update-investor-website").val(paramInvestorDataObj.website);
    $("#inp-update-investor-note").val(paramInvestorDataObj.note);
}
//hàm thu thập update data
function getUpdateInvestorData(paramInvestorDataObj) {
    paramInvestorDataObj.name = $("#inp-update-investor-name").val().trim();
    paramInvestorDataObj.description = $("#inp-update-investor-description").val().trim();
    paramInvestorDataObj.projects = $("#inp-update-investor-projects").val().trim();
    paramInvestorDataObj.address = $("#inp-update-investor-address").val().trim();
    paramInvestorDataObj.phone = $("#inp-update-investor-phone").val().trim();
    paramInvestorDataObj.phone2 = $("#inp-update-investor-phone2").val().trim();
    paramInvestorDataObj.fax = $("#inp-update-investor-fax").val().trim();
    paramInvestorDataObj.email = $("#inp-update-investor-email").val().trim();
    paramInvestorDataObj.website = $("#inp-update-investor-website").val().trim();
    paramInvestorDataObj.note = $("#inp-update-investor-note").val().trim();
}
//hàm xử lý front-end khi update data thành công
function handleUpdateInvestorSuccess() {
    alert("Cập nhật thành công!");
    getAllinvestors();
    $("#update-investor-modal").modal("hide");
}
//hàm xử lý front-end khi delete thành công
function handleDeleteInvestorSuccess() {
    alert("Xóa thành công!");
    getAllinvestors();
    $("#delete-investor-modal").modal("hide");
}