"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/customers/";
// biến toàn cục để lưu trữ id đang dc update or delete. Mặc định = 0
var gCustomerId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gCUSTOMER_COLS = ["id", "contactTitle", "contactName", "address", "mobile", "email", "createBy", "updateBy", "createDate", "updateDate", "action"];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gID_COL = 0;
const gCONTACT_TITLE_COL = 1;
const gCONTACT_NAME_COL = 2;
const gADDRESS_COL = 3;
const gMOBILE_COL = 4;
const gEMAIL_COL = 5;
const gCREATE_BY_COL = 6;
const gUPDATE_BY_COL = 7;
const gCREATE_DATE_COL = 8;
const gUPDATE_DATE_COL = 9;
const gACTION_COL = 10;

//khai báo DataTable & mapping columns
var gCustomerTable = $("#customer-table").DataTable({
    columns: [
        { data: gCUSTOMER_COLS[gID_COL] },
        { data: gCUSTOMER_COLS[gCONTACT_TITLE_COL] },
        { data: gCUSTOMER_COLS[gCONTACT_NAME_COL] },
        { data: gCUSTOMER_COLS[gADDRESS_COL] },
        { data: gCUSTOMER_COLS[gMOBILE_COL] },
        { data: gCUSTOMER_COLS[gEMAIL_COL] },
        { data: gCUSTOMER_COLS[gCREATE_BY_COL] },
        { data: gCUSTOMER_COLS[gUPDATE_BY_COL] },
        { data: gCUSTOMER_COLS[gCREATE_DATE_COL] },
        { data: gCUSTOMER_COLS[gUPDATE_DATE_COL] },
        { data: gCUSTOMER_COLS[gACTION_COL] }
    ],
    columnDefs: [
        {
            targets: gACTION_COL,
            defaultContent: `
            <i class="fas fa-edit edit-customer" style="cursor:pointer;color:blue;"></i>
            <i class="fas fa-trash delete-customer" style="cursor:pointer;color:red;"></i>
          `
        }
    ]
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
});
//2 - C: gán sự kiện Create-thêm mới
$("#btn-add-new-customer").on("click", function () {
    onBtnAddNewCustomerClick();
});
//3 - U: gán sự kiện update-sửa
$("#customer-table").on("click", ".edit-customer", function () {
    onBtnEditCustomerClick(this);
});
//4 - D: gán sự kiện delete-xóa
$("#customer-table").on("click", ".delete-customer", function () {
    onBtnDeleteCustomerClick(this);
});

//gán sự kiện cho nút Create(trên modal)
$("#btn-create-customer").on("click", function () {
    onBtnCreateCustomerClick();
});
//gán sự kiện cho nút Update(trên modal)
$("#btn-update-customer").on("click", function () {
    onBtnUpdateCustomerClick();
});
//gán sự kiện cho nút Delete(trên modal)
$("#btn-confirm-delete-customer").on("click", function () {
    onBtnDeleteCustomerConfirmClick();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    //1 - R: read/load to table
    getAllCustomers();
}
//hàm xử lý sự kiện nút Thêm mới
function onBtnAddNewCustomerClick() {
    //hiển thị modal trắng lên
    $("#create-customer-modal").modal("show");
}
//hàm xử lý sự kiện Update từ table
function onBtnEditCustomerClick(paramEdit) {
    //lưu thông tin Id đang dc edit vào biến toàn cục
    gCustomerId = getCustomerIdFromButton(paramEdit);
    console.log("Id của obj tương ứng = " + gCustomerId);
    // load dữ liệu vào các trường dữ liệu trong modal
    getCustomerDataByCustomerId(gCustomerId);
}
//hàm xử lý sự kiện Delete từ table
function onBtnDeleteCustomerClick(paramDelete) {
    //lưu thông tin Id đang dc xóa vào biến toàn cục
    gCustomerId = getCustomerIdFromButton(paramDelete);
    console.log("Id của obj tương ứng = " + gCustomerId);
    $("#delete-customer-modal").modal("show");
}

//hàm xử lý sự kiện nút Insert(trên modal)
function onBtnCreateCustomerClick() {
    //khai báo obj chứa data
    var vRequestDataObj = {
        contactTitle: "",
        contactName: "",
        address: "",
        mobile: "",
        email: "",
        note: ""
    }
    //b1: get data
    getInsertCustomerData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateCustomerData(vRequestDataObj);
    //ghi ra console value trả ra của hàm validate
    console.log(vIsValid);
    if (vIsValid) {
        //b3: insert data
        $.ajax({
            url: gBASE_URL,
            type: "POST",
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleInsertCustomerSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

//hàm xử lý sự kiện nút Update(trên modal)
function onBtnUpdateCustomerClick() {
    //khai báo obj chứa data
    var vRequestDataObj = {
        contactTitle: "",
        contactName: "",
        address: "",
        mobile: "",
        email: "",
        note: ""
    }
    //b1: get data
    getUpdateCustomerData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateCustomerData(vRequestDataObj);
    if (vIsValid) {
        //b3: update data
        $.ajax({
            url: gBASE_URL + gCustomerId,
            type: "PUT",
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleUpdateCustomerSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

// hàm xử lý sự kiện delete trên modal
function onBtnDeleteCustomerConfirmClick() {
    $.ajax({
        url: gBASE_URL + gCustomerId,
        type: "DELETE",
        contentType: "application/json;charset-UTF-8",
        success: function (paramRes) {
            // B4: xử lý front-end
            handleDeleteCustomerSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to DataTable
function loadDataToTable(paramCustomer) {
    gCustomerTable.clear();
    gCustomerTable.rows.add(paramCustomer);
    gCustomerTable.draw();
}
//hàm gọi api để lấy all list
function getAllCustomers() {
    $.ajax({
        url: gBASE_URL,
        type: "GET",
        success: function (paramRes) {
            //ghi response ra console
            console.log(paramRes);
            loadDataToTable(paramRes);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

//hàm dựa vào button detail (edit or delete) xác định dc id obj
function getCustomerIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    var vCustomerRowData = gCustomerTable.row(vTableRow).data();
    return vCustomerRowData.id;
}

//hàm thu thập insert data
function getInsertCustomerData(paramObj) {
    paramObj.contactTitle = $("#inp-create-customer-contact").val().trim();
    paramObj.contactName = $("#inp-create-customer-name").val().trim();
    paramObj.address = $("#inp-create-customer-address").val().trim();
    paramObj.mobile = $("#inp-create-customer-phone").val().trim();
    paramObj.email = $("#inp-create-customer-email").val().trim();
    paramObj.note = $("#inp-create-customer-note").val().trim();
}
//hàm validate data
function validateCustomerData(paramObj) {
    if (paramObj.email !== "") {
        var emailPattern = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
        if (!emailPattern.test(paramObj.email)) {
            alert("Email không đúng định dạng!");
            return false;
        }
    }

    // if (paramObj.mobile !== "") {
    //     // Kiểm tra mobile là duy nhất (unique)
    //     var vUniqueMobile = getUniqueMobilesFromDatabase();
    //     if (vUniqueMobile.includes(paramObj.mobile)) {
    //         alert("Sđt đã tồn tại!");
    //         return false;
    //     }
    // }

    return true;
}
//hàm xử lý front-end khi thêm obj thành công
function handleInsertCustomerSuccess() {
    alert("Thêm mới thành công!");
    getAllCustomers();
    resetCreateCustomerForm();
    $("#create-customer-modal").modal("hide");
}
//hàm xóa trắng form create
function resetCreateCustomerForm() {
    $("#inp-create-customer-contact").val("");
    $("#inp-create-customer-name").val("");
    $("#inp-create-customer-address").val("");
    $("#inp-create-customer-phone").val("");
    $("#inp-create-customer-email").val("");
    $("#inp-create-customer-note").val("");
}

//hàm load data lên update modal form
function getCustomerDataByCustomerId(paramCustomerId) {
    $.ajax({
        url: gBASE_URL + paramCustomerId,
        type: "GET",
        success: function (paramCustomerId) {
            showDataToUpdateModal(paramCustomerId);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
    // hiển thị modal lên
    $("#update-customer-modal").modal("show");
}
//hàm show data lên update modal form
function showDataToUpdateModal(paramDataObj) {
    $("#inp-update-customer-id").val(paramDataObj.id);
    $("#inp-update-customer-contact").val(paramDataObj.contactTitle);
    $("#inp-update-customer-name").val(paramDataObj.contactName);
    $("#inp-update-customer-address").val(paramDataObj.address);
    $("#inp-update-customer-phone").val(paramDataObj.mobile);
    $("#inp-update-customer-email").val(paramDataObj.email);
    $("#inp-update-customer-note").val(paramDataObj.note);
}
//hàm thu thập update data
function getUpdateCustomerData(paramDataObj) {
    paramDataObj.contactTitle = $("#inp-update-customer-contact").val().trim();
    paramDataObj.contactName = $("#inp-update-customer-name").val().trim();
    paramDataObj.address = $("#inp-update-customer-address").val().trim();
    paramDataObj.mobile = $("#inp-update-customer-phone").val().trim();
    paramDataObj.email = $("#inp-update-customer-email").val().trim();
    paramDataObj.note = $("#inp-update-customer-note").val().trim();
}
//hàm xử lý front-end khi update data thành công
function handleUpdateCustomerSuccess() {
    alert("Cập nhật thành công!");
    getAllCustomers();
    $("#update-customer-modal").modal("hide");
}
//hàm xử lý front-end khi delete thành công
function handleDeleteCustomerSuccess() {
    alert("Xóa thành công!");
    getAllCustomers();
    $("#delete-customer-modal").modal("hide");
}