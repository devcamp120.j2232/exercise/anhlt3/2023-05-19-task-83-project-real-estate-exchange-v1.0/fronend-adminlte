"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/region-links/";
// biến toàn cục để lưu trữ id đang dc update or delete. Mặc định = 0
var gRLId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gREGION_LINK_COLS = ["id", "name", "lat", "lng", "action"];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gRL_ID_COL = 0;
const gRL_NAME_COL = 1;
const gRL_LAT_COL = 2;
const gRL_LNG_COL = 3;
const gRL_ACTION_COL = 4;

//khai báo DataTable & mapping columns
var gRLTable = $("#regionlink-table").DataTable({
    columns: [
        { data: gREGION_LINK_COLS[gRL_ID_COL] },
        { data: gREGION_LINK_COLS[gRL_NAME_COL] },
        { data: gREGION_LINK_COLS[gRL_LAT_COL] },
        { data: gREGION_LINK_COLS[gRL_LNG_COL] },
        { data: gREGION_LINK_COLS[gRL_ACTION_COL] }
    ],
    columnDefs: [
        {
            targets: gRL_ACTION_COL,
            defaultContent: `
            <i class="fas fa-edit edit-rl" style="cursor:pointer;color:blue;"></i>
            <i class="fas fa-trash delete-rl" style="cursor:pointer;color:red;"></i>
          `
        }
    ]
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
});
//2 - C: gán sự kiện Create-thêm mới
$("#btn-add-new-regionlink").on("click", function () {
    onBtnAddNewRLClick();
});
//3 - U: gán sự kiện update-sửa
$("#regionlink-table").on("click", ".edit-rl", function () {
    onBtnEditRLClick(this);
});
//4 - D: gán sự kiện delete-xóa
$("#regionlink-table").on("click", ".delete-rl", function () {
    onBtnDeleteRLClick(this);
});

//gán sự kiện cho nút Create(trên modal)
$("#btn-create-regionlink").on("click", function () {
    onBtnCreateRLClick();
});
//gán sự kiện cho nút Update(trên modal)
$("#btn-update-regionlink").on("click", function () {
    onBtnUpdateRLClick();
});
//gán sự kiện cho nút Delete(trên modal)
$("#btn-confirm-delete-regionlink").on("click", function () {
    onBtnDeleteRLConfirmClick();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    //1 - R: read/load to table
    getAllRegionLinks();
}
//hàm xử lý sự kiện nút Thêm mới
function onBtnAddNewRLClick() {
    //hiển thị modal trắng lên
    $("#create-regionlink-modal").modal("show");
}
//hàm xử lý sự kiện Update từ table
function onBtnEditRLClick(paramEdit) {
    //lưu thông tin Id đang dc edit vào biến toàn cục
    gRLId = getRLIdFromButton(paramEdit);
    console.log("Id của obj tương ứng = " + gRLId);
    // load dữ liệu vào các trường dữ liệu trong modal
    getRLDataByRLId(gRLId);
}
//hàm xử lý sự kiện Delete từ table
function onBtnDeleteRLClick(paramDelete) {
    //lưu thông tin Id đang dc xóa vào biến toàn cục
    gRLId = getRLIdFromButton(paramDelete);
    console.log("Id của obj tương ứng = " + gRLId);
    $("#delete-regionlink-modal").modal("show");
}

//hàm xử lý sự kiện nút Insert(trên modal)
function onBtnCreateRLClick() {
    //khai báo obj chứa data
    var vRequestDataObj = {
        name: "",
        description: "",
        photo: "",
        address: "",
        lat: "",
        lng: ""
    }
    //b1: get data
    getInsertRLData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateRLData(vRequestDataObj);
    //ghi ra console value trả ra của hàm validate
    console.log(vIsValid);
    if (vIsValid) {
        //b3: insert data
        $.ajax({
            url: gBASE_URL,
            type: "POST",
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleInsertRLSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

//hàm xử lý sự kiện nút Update(trên modal)
function onBtnUpdateRLClick() {
    //khai báo obj chứa data
    var vRequestDataObj = {
        name: "",
        description: "",
        photo: "",
        address: "",
        lat: "",
        lng: ""
    }
    //b1: get data
    getUpdateRLData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateRLData(vRequestDataObj);
    if (vIsValid) {
        //b3: update data
        $.ajax({
            url: gBASE_URL + gRLId,
            type: "PUT",
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleUpdateRLSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

// hàm xử lý sự kiện delete trên modal
function onBtnDeleteRLConfirmClick() {
    $.ajax({
        url: gBASE_URL + gRLId,
        type: "DELETE",
        contentType: "application/json;charset-UTF-8",
        success: function (paramRes) {
            // B4: xử lý front-end
            handleDeleteRLSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to DataTable
function loadDataToTable(paramRL) {
    gRLTable.clear();
    gRLTable.rows.add(paramRL);
    gRLTable.draw();
}
//hàm gọi api để lấy all list
function getAllRegionLinks() {
    $.ajax({
        url: gBASE_URL,
        type: "GET",
        success: function (paramRL) {
            //ghi response ra console
            console.log(paramRL);
            loadDataToTable(paramRL);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
//hàm dựa vào button detail (edit or delete) xác định dc id obj
function getRLIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    var vRLRowData = gRLTable.row(vTableRow).data();
    return vRLRowData.id;
}

//hàm thu thập insert data
function getInsertRLData(paramRLObj) {
    paramRLObj.name = $("#inp-create-regionlink-name").val().trim();
    paramRLObj.description = $("#inp-create-regionlink-description").val().trim();
    var vPhotoInput = $("#inp-create-regionlink-photo");
    var vFileName = vPhotoInput.val().split('\\').pop().split('/').pop();
    paramRLObj.photo = vFileName;
    paramRLObj.address = $("#inp-create-regionlink-address").val().trim();
    paramRLObj.lat = $("#inp-create-regionlink-lat").val().trim();
    paramRLObj.lng = $("#inp-create-regionlink-lng").val().trim();
}
//hàm validate data
function validateRLData(paramRLObj) {
    if (paramRLObj.name === "") {
        alert("Chưa nhập tên!");
        return false;
    }

    return true;
}
//hàm xử lý front-end khi thêm obj thành công
function handleInsertRLSuccess() {
    alert("Thêm mới thành công!");
    getAllRegionLinks();
    resetCreateRLForm();
    $("#create-regionlink-modal").modal("hide");
}
//hàm xóa trắng form create
function resetCreateRLForm() {
    $("#inp-create-regionlink-name").val("");
    $("#inp-create-regionlink-description").val("");
    $("#inp-create-regionlink-photo").val("");
    $("#inp-create-regionlink-address").val("");
    $("#inp-create-regionlink-lat").val("");
    $("#inp-create-regionlink-lng").val(""); 
}

//hàm load data lên update modal form
function getRLDataByRLId(paramRLId) {
    $.ajax({
        url: gBASE_URL + paramRLId,
        type: "GET",
        success: function (paramRLId) {
            showRLDataToUpdateModal(paramRLId);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
    // hiển thị modal lên
    $("#update-regionlink-modal").modal("show");
}
//hàm show data lên update modal form
function showRLDataToUpdateModal(paramRLDataObj) {
    $("#inp-update-regionlink-id").val(paramRLDataObj.id);
    $("#inp-update-regionlink-name").val(paramRLDataObj.name);
    $("#inp-update-regionlink-description").val(paramRLDataObj.description);
    // Hiển thị tên file trong một thẻ <span>
    var vFileName = paramRLDataObj.photo.split('/').pop();
    $("#file-label").text(vFileName);
    $("#inp-update-regionlink-address").val(paramRLDataObj.address);
    $("#inp-update-regionlink-lat").val(paramRLDataObj.lat);
    $("#inp-update-regionlink-lng").val(paramRLDataObj.lng);
}
//hàm thu thập update data
function getUpdateRLData(paramRLDataObj) {
    paramRLDataObj.name = $("#inp-update-regionlink-name").val().trim();
    paramRLDataObj.description = $("#inp-update-regionlink-description").val().trim();
    var vPhotoInput = $("#inp-update-regionlink-photo");
    var vFileName = vPhotoInput.val().split('\\').pop().split('/').pop();
    paramRLDataObj.photo = vFileName;
    paramRLDataObj.address = $("#inp-update-regionlink-address").val().trim();
    paramRLDataObj.lat = $("#inp-update-regionlink-lat").val().trim();
    paramRLDataObj.lng = $("#inp-update-regionlink-lng").val().trim();   
}
//hàm xử lý front-end khi update data thành công
function handleUpdateRLSuccess() {
    alert("Cập nhật thành công!");
    getAllRegionLinks();
    $("#update-regionlink-modal").modal("hide");
}
//hàm xử lý front-end khi delete thành công
function handleDeleteRLSuccess() {
    alert("Xóa thành công!");
    getAllRegionLinks();
    $("#delete-regionlink-modal").modal("hide");
}