"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/employees/";
// biến toàn cục để lưu trữ id đang dc update or delete. Mặc định = 0
var gEmployeeId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gEMPLOYEE_COLS = ["id",
    "lastName",
    "firstName",
    "title",
    "titleOfCourtesy",
    "birthDate",
    "hireDate",
    "address",
    "city",
    "region",
    "postalCode",
    "country",
    "homePhone",
    "extension",
    "photo",
    "reportsTo",
    "username",
    "password",
    "email",
    "activated",
    "userLevel",
    "action"
];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gID_COL = 0;
const gFIRST_NAME_COL = 1;
const gLAST_NAME_COL = 2;
const gTITLE_COL = 3;
const gTITLE_COURSETY_COL = 4;
const gBIRTH_DATE_COL = 5;
const gHIRE_DATE_COL = 6;
const gADDRESS_COL = 7;
const gCITY_COL = 8;
const gREGION_COL = 9;
const gPOSTAL_CODE_COL = 10;
const gCOUNTRY_COL = 11;
const gHOME_PHONE_COL = 12;
const gEXTENSION_COL = 13;
const gPHOTO_COL = 14;
const gREPORT_TO_COL = 15;
const gUSERNAME_COL = 16;
const gPASSWORD_COL = 17;
const gEMAIL_COL = 18;
const gACTIVATED_COL = 19;
const gUSER_LEVEL_COL = 20;
const gACTION_COL = 21;

//khai báo DataTable & mapping columns
var gEmployeeTable = $("#employee-table").DataTable({
    columns: [
        { data: gEMPLOYEE_COLS[gID_COL] },
        { data: gEMPLOYEE_COLS[gFIRST_NAME_COL] },
        { data: gEMPLOYEE_COLS[gLAST_NAME_COL] },
        { data: gEMPLOYEE_COLS[gTITLE_COL] },
        { data: gEMPLOYEE_COLS[gTITLE_COURSETY_COL] },
        { data: gEMPLOYEE_COLS[gBIRTH_DATE_COL] },
        { data: gEMPLOYEE_COLS[gHIRE_DATE_COL] },
        { data: gEMPLOYEE_COLS[gADDRESS_COL] },
        { data: gEMPLOYEE_COLS[gCITY_COL] },
        { data: gEMPLOYEE_COLS[gREGION_COL] },
        { data: gEMPLOYEE_COLS[gPOSTAL_CODE_COL] },
        { data: gEMPLOYEE_COLS[gCOUNTRY_COL] },
        { data: gEMPLOYEE_COLS[gHOME_PHONE_COL] },
        { data: gEMPLOYEE_COLS[gEXTENSION_COL] },
        { data: gEMPLOYEE_COLS[gPHOTO_COL] },
        { data: gEMPLOYEE_COLS[gREPORT_TO_COL] },
        { data: gEMPLOYEE_COLS[gUSERNAME_COL] },
        { data: gEMPLOYEE_COLS[gPASSWORD_COL] },
        { data: gEMPLOYEE_COLS[gEMAIL_COL] },
        { data: gEMPLOYEE_COLS[gACTIVATED_COL] },
        { data: gEMPLOYEE_COLS[gUSER_LEVEL_COL] },
        { data: gEMPLOYEE_COLS[gACTION_COL] }
    ],
    columnDefs: [
        {
            targets: gACTION_COL,
            defaultContent: `
            <i class="fas fa-edit edit-employee" style="cursor:pointer;color:blue;"></i>
            <i class="fas fa-trash delete-employee" style="cursor:pointer;color:red;"></i>
          `
        }
    ]
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
});
//2 - C: gán sự kiện Create-thêm mới
$("#btn-add-new-employee").on("click", function () {
    onBtnAddNewEmployeeClick();
});
//3 - U: gán sự kiện update-sửa
$("#employee-table").on("click", ".edit-employee", function () {
    onBtnEditEmployeeClick(this);
});
//4 - D: gán sự kiện delete-xóa
$("#employee-table").on("click", ".delete-employee", function () {
    onBtnDeleteEmployeeClick(this);
});

//gán sự kiện cho nút Create(trên modal)
$("#btn-create-employee").on("click", function () {
    onBtnCreateEmployeeClick();
});
//gán sự kiện cho nút Update(trên modal)
$("#btn-update-employee").on("click", function () {
    onBtnUpdateEmployeeClick();
});
//gán sự kiện cho nút Delete(trên modal)
$("#btn-confirm-delete-employee").on("click", function () {
    onBtnDeleteEmployeeConfirmClick();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    //1 - R: read/load to table
    getAllEmployees();
}
//hàm xử lý sự kiện nút Thêm mới
function onBtnAddNewEmployeeClick() {
    //hiển thị modal trắng lên
    $("#create-employee-modal").modal("show");
}
//hàm xử lý sự kiện Update từ table
function onBtnEditEmployeeClick(paramEdit) {
    //lưu thông tin Id đang dc edit vào biến toàn cục
    gEmployeeId = getEmployeeIdFromButton(paramEdit);
    console.log("Id của obj tương ứng = " + gEmployeeId);
    // load dữ liệu vào các trường dữ liệu trong modal
    getEmployeeDataByEmployeeId(gEmployeeId);
}
//hàm xử lý sự kiện Delete từ table
function onBtnDeleteEmployeeClick(paramDelete) {
    //lưu thông tin Id đang dc xóa vào biến toàn cục
    gEmployeeId = getEmployeeIdFromButton(paramDelete);
    console.log("Id của obj tương ứng = " + gEmployeeId);
    $("#delete-employee-modal").modal("show");
}

//hàm xử lý sự kiện nút Insert(trên modal)
function onBtnCreateEmployeeClick() {
    //khai báo obj chứa data
    var vRequestDataObj = {
        lastName: "",
        firstName: "",
        title: "",
        titleOfCourtesy: "",
        birthDate: "",
        hireDate: "",
        address: "",
        city: "",
        region: "",
        postalCode: "",
        country: "",
        homePhone: "",
        extension: "",
        photo: "",
        notes: "",
        reportsTo: "",
        username: "",
        password: "",
        email: "",
        activated: "",
        profile: "",
        userLevel: ""
    }
    //b1: get data
    getInsertEmployeeData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateEmployeeData(vRequestDataObj);
    //ghi ra console value trả ra của hàm validate
    console.log(vIsValid);
    if (vIsValid) {
        //b3: insert data
        $.ajax({
            url: gBASE_URL,
            type: "POST",
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleInsertEmployeeSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

//hàm xử lý sự kiện nút Update(trên modal)
function onBtnUpdateEmployeeClick() {
    //khai báo obj chứa data
    var vRequestDataObj = {
        lastName: "",
        firstName: "",
        title: "",
        titleOfCourtesy: "",
        birthDate: "",
        hireDate: "",
        address: "",
        city: "",
        region: "",
        postalCode: "",
        country: "",
        homePhone: "",
        extension: "",
        photo: "",
        notes: "",
        reportsTo: "",
        username: "",
        password: "",
        email: "",
        activated: "",
        profile: "",
        userLevel: ""
    }
    //b1: get data
    getUpdateEmployeeData(vRequestDataObj);
    //b2: validate data
    var vIsValid = validateEmployeeData(vRequestDataObj);
    if (vIsValid) {
        //b3: update data
        $.ajax({
            url: gBASE_URL + gEmployeeId,
            type: "PUT",
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(vRequestDataObj),
            success: function (paramRes) {
                console.log(paramRes);
                //b4: xử lý front-end
                handleUpdateEmployeeSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}

// hàm xử lý sự kiện delete trên modal
function onBtnDeleteEmployeeConfirmClick() {
    $.ajax({
        url: gBASE_URL + gEmployeeId,
        type: "DELETE",
        contentType: "application/json;charset-UTF-8",
        success: function (paramRes) {
            // B4: xử lý front-end
            handleDeleteEmployeeSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to DataTable
function loadDataToTable(paramEmployee) {
    gEmployeeTable.clear();
    gEmployeeTable.rows.add(paramEmployee);
    gEmployeeTable.draw();
}
//hàm gọi api để lấy all list
function getAllEmployees() {
    $.ajax({
        url: gBASE_URL,
        type: "GET",
        success: function (paramEmployee) {
            //ghi response ra console
            console.log(paramEmployee);
            loadDataToTable(paramEmployee);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
//hàm dựa vào button detail (edit or delete) xác định dc id obj
function getEmployeeIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    var vEmployeeRowData = gEmployeeTable.row(vTableRow).data();
    return vEmployeeRowData.id;
}

//hàm thu thập insert data
function getInsertEmployeeData(paramEmployeeObj) {
    paramEmployeeObj.lastName = $("#inp-create-employee-lastName").val().trim();
    paramEmployeeObj.firstName = $("#inp-create-employee-firstName").val().trim();
    paramEmployeeObj.title = $("#inp-create-employee-title").val().trim();
    paramEmployeeObj.titleOfCourtesy = $("#inp-create-employee-titleOfCourtesy").val().trim();
    paramEmployeeObj.birthDate = $("#inp-create-employee-birthDate").val().trim();
    paramEmployeeObj.hireDate = $("#inp-create-employee-hireDate").val().trim();
    paramEmployeeObj.address = $("#inp-create-employee-address").val().trim();
    paramEmployeeObj.city = $("#inp-create-employee-city").val().trim();
    paramEmployeeObj.region = $("#inp-create-employee-region").val().trim();
    paramEmployeeObj.postalCode = $("#inp-create-employee-postalCode").val().trim();
    paramEmployeeObj.country = $("#inp-create-employee-country").val().trim();
    paramEmployeeObj.homePhone = $("#inp-create-employee-homePhone").val().trim();
    paramEmployeeObj.extension = $("#inp-create-employee-extension").val().trim();
    var vPhotoInput = $("#inp-create-employee-photo");
    var vFileName = vPhotoInput.val().split('\\').pop().split('/').pop();
    paramEmployeeObj.photo = vFileName;
    paramEmployeeObj.notes = $("#inp-create-employee-notes").val().trim();
    paramEmployeeObj.reportsTo = $("#inp-create-employee-reportsTo").val().trim();
    paramEmployeeObj.username = $("#inp-create-employee-username").val().trim();
    paramEmployeeObj.password = $("#inp-create-employee-password").val().trim();
    paramEmployeeObj.email = $("#inp-create-employee-email").val().trim();
    paramEmployeeObj.activated = $("#select-create-employee-activated").val();
    paramEmployeeObj.profile = $("#inp-create-employee-profile").val().trim();
    paramEmployeeObj.userLevel = parseInt($("#select-create-employee-userLevel").val());
}
//hàm validate data
function validateEmployeeData(paramEmployeeObj) {
    if (paramEmployeeObj.lastName === "") {
        alert("Chưa nhập last name!");
        return false;
    }
    if (paramEmployeeObj.firstName === "") {
        alert("Chưa nhập first name!");
        return false;
    }
    if (paramEmployeeObj.username === "") {
        alert("Chưa nhập username!");
        return false;
    }
    if (paramEmployeeObj.password === "") {
        alert("Chưa nhập password!");
        return false;
    }
    if (paramEmployeeObj.email === "") {
        alert("Chưa nhập email!");
        return false;
    }

    var emailPattern = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
    if (!emailPattern.test(paramEmployeeObj.email)) {
        alert("Email không đúng định dạng!");
        return false;
    }

    return true;
}
//hàm xử lý front-end khi thêm obj thành công
function handleInsertEmployeeSuccess() {
    alert("Thêm mới thành công!");
    getAllEmployees();
    resetCreateEmployeeForm();
    $("#create-employee-modal").modal("hide");
}
//hàm xóa trắng form create
function resetCreateEmployeeForm() {
    $("#inp-create-employee-lastName").val("");
    $("#inp-create-employee-firstName").val("");
    $("#inp-create-employee-title").val("");
    $("#inp-create-employee-titleOfCourtesy").val("");
    $("#inp-create-employee-birthDate").val("");
    $("#inp-create-employee-hireDate").val("");
    $("#inp-create-employee-address").val("");
    $("#inp-create-employee-city").val("");
    $("#inp-create-employee-region").val("");
    $("#inp-create-employee-postalCode").val("");
    $("#inp-create-employee-country").val("");
    $("#inp-create-employee-homePhone").val("");
    $("#inp-create-employee-extension").val("");
    $("#inp-create-employee-photo").val("");
    $("#inp-create-employee-notes").val("");
    $("#inp-create-employee-reportsTo").val("");
    $("#inp-create-employee-username").val("");
    $("#inp-create-employee-password").val("");
    $("#inp-create-employee-email").val("");
    $("#select-create-employee-activated").val("");
    $("#inp-create-employee-profile").val("");
    $("#select-create-employee-userLevel").val("");
}

//hàm load data lên update modal form
function getEmployeeDataByEmployeeId(paramEmployeeId) {
    $.ajax({
        url: gBASE_URL + paramEmployeeId,
        type: "GET",
        success: function (paramEmployeeId) {
            showEmployeeDataToUpdateModal(paramEmployeeId);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
    // hiển thị modal lên
    $("#update-employee-modal").modal("show");
}
//hàm show data lên update modal form
function showEmployeeDataToUpdateModal(paramEmployeeDataObj) {
    $("#inp-update-employee-id").val(paramEmployeeDataObj.id);
    $("#inp-update-employee-lastName").val(paramEmployeeDataObj.lastName);
    $("#inp-update-employee-firstName").val(paramEmployeeDataObj.firstName);
    $("#inp-update-employee-title").val(paramEmployeeDataObj.title);
    $("#inp-update-employee-titleOfCourtesy").val(paramEmployeeDataObj.titleOfCourtesy);
    $("#inp-update-employee-birthDate").val(paramEmployeeDataObj.birthDate);
    $("#inp-update-employee-hireDate").val(paramEmployeeDataObj.hireDate);
    $("#inp-update-employee-address").val(paramEmployeeDataObj.address);
    $("#inp-update-employee-city").val(paramEmployeeDataObj.city);
    $("#inp-update-employee-region").val(paramEmployeeDataObj.region);
    $("#inp-update-employee-postalCode").val(paramEmployeeDataObj.postalCode);
    $("#inp-update-employee-country").val(paramEmployeeDataObj.country);
    $("#inp-update-employee-homePhone").val(paramEmployeeDataObj.homePhone);
    $("#inp-update-employee-extension").val(paramEmployeeDataObj.extension);
    // Hiển thị tên file trong một thẻ <span>
    var vFileName = paramEmployeeDataObj.photo.split('/').pop();
    $("#file-label").text(vFileName);
    $("#inp-update-employee-notes").val(paramEmployeeDataObj.notes);
    $("#inp-update-employee-reportsTo").val(paramEmployeeDataObj.reportsTo);
    $("#inp-update-employee-username").val(paramEmployeeDataObj.username);
    $("#inp-update-employee-password").val(paramEmployeeDataObj.password);
    $("#inp-update-employee-email").val(paramEmployeeDataObj.email);
    $("#select-update-employee-activated").val(paramEmployeeDataObj.activated);
    $("#inp-update-employee-profile").val(paramEmployeeDataObj.profile);
    $("#select-update-employee-userLevel").val(paramEmployeeDataObj.userLevel);
}
//hàm thu thập update data
function getUpdateEmployeeData(paramEmployeeDataObj) {
    paramEmployeeDataObj.lastName = $("#inp-update-employee-lastName").val().trim();
    paramEmployeeDataObj.firstName = $("#inp-update-employee-firstName").val().trim();
    paramEmployeeDataObj.natitleme = $("#inp-update-employee-title").val().trim();
    paramEmployeeDataObj.titleOfCourtesy = $("#inp-update-employee-titleOfCourtesy").val().trim();
    paramEmployeeDataObj.birthDate = $("#inp-update-employee-birthDate").val().trim();
    paramEmployeeDataObj.hireDate = $("#inp-update-employee-hireDate").val().trim();
    paramEmployeeDataObj.address = $("#inp-update-employee-address").val().trim();
    paramEmployeeDataObj.city = $("#inp-update-employee-city").val().trim();
    paramEmployeeDataObj.region = $("#inp-update-employee-region").val().trim();
    paramEmployeeDataObj.postalCode = $("#inp-update-employee-postalCode").val().trim();
    paramEmployeeDataObj.country = $("#inp-update-employee-country").val().trim();
    paramEmployeeDataObj.homePhone = $("#inp-update-employee-homePhone").val().trim();
    paramEmployeeDataObj.extension = $("#inp-update-employee-extension").val().trim();
    var vPhotoInput = $("#inp-update-employee-photo");
    var vFileName = vPhotoInput.val().split('\\').pop().split('/').pop();
    paramEmployeeDataObj.photo = vFileName;
    paramEmployeeDataObj.notes = $("#inp-update-employee-notes").val().trim();
    paramEmployeeDataObj.reportsTo = $("#inp-update-employee-reportsTo").val().trim();
    paramEmployeeDataObj.username = $("#inp-update-employee-username").val().trim();
    paramEmployeeDataObj.password = $("#inp-update-employee-password").val().trim();
    paramEmployeeDataObj.email = $("#inp-update-employee-email").val().trim();
    paramEmployeeDataObj.activated = $("#select-update-employee-activated").val();
    paramEmployeeDataObj.profile = $("#inp-update-employee-profile").val().trim();
    paramEmployeeDataObj.userLevel = parseInt($("#select-update-employee-userLevel").val());
}
//hàm xử lý front-end khi update data thành công
function handleUpdateEmployeeSuccess() {
    alert("Cập nhật thành công!");
    getAllEmployees();
    $("#update-employee-modal").modal("hide");
}
//hàm xử lý front-end khi delete thành công
function handleDeleteEmployeeSuccess() {
    alert("Xóa thành công!");
    getAllEmployees();
    $("#delete-employee-modal").modal("hide");
}