$(document).ready(function () {
    // Sự kiện submit form
    $("form").on("submit", function (event) {
        event.preventDefault(); // Ngăn chặn hành vi submit mặc định

        var username = $("#inp-username").val().trim();
        var email = $("#inp-email").val().trim();
        var password = $("#inp-password").val().trim();
        var confirmPassword = $("#inp-confirm-password").val().trim();

        if (validateForm(username, email, password, confirmPassword)) {
            signupForm(username, email, password);
        }
    });

    function signupForm(username, email, password) {
        var signupUrl = "http://127.0.0.1:8080/api/auth/signup";

        var signUpData = {
            username: username,
            email: email,
            password: password
        };

        $.ajax({
            url: signupUrl,
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(signUpData),
            success: function (responseObject) {
                responseHandler(responseObject);
            },
            error: function (xhr) {
                var errorMessage = xhr.responseJSON.message;
                showError(errorMessage);
            }
        });
    }

    function responseHandler(data) {
        console.log(data); // Hiển thị dữ liệu API trả về ra console
        alert("Sign up success!"); // Hiển thị thông báo cho người dùng
        $("form")[0].reset(); // Reset lại form
    }

    function showError(message) {
        console.error(message);
    }

    function validateForm(username, email, password, confirmPassword) {
        if (username === "") {
            alert("Please enter a username.");
            return false;
        }

        if (email === "") {
            alert("Please enter an email.");
            return false;
        }

        if (password === "") {
            alert("Please enter a password.");
            return false;
        }

        if (password !== confirmPassword) {
            alert("Passwords do not match.");
            return false;
        }
        return true;
    }
});







